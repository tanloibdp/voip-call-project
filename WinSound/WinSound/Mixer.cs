﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WinSound
{
	/// <summary>
	/// Mixer
	/// </summary>
	public class Mixer
	{
		/// <summary>
		/// MixBytes
		/// </summary>
		/// <param name="?"></param>
		/// <param name="maxListCount"></param>
		/// <param name="BitsPerSample"></param>
		/// <returns></returns>
		public static List<Byte> MixBytes(List<List<Byte>> listList, int BitsPerSample)
		{
			//Result
			List<Int32> list16 = new List<Int32>();
			List<Int32> list16Abs = new List<Int32>();
			int maximum = 0;

			//Finished
			return MixBytes_Intern(listList, BitsPerSample, out list16, out list16Abs, out maximum);
		}
		/// <summary>
		/// MixBytes 
		/// </summary>
		/// <param name="listList"></param>
		/// <param name="BitsPerSample"></param>
		/// <param name="listLinear"></param>
		/// <returns></returns>
		public static List<Byte> MixBytes(List<List<Byte>> listList, int BitsPerSample, out List<Int32> listLinear, out List<Int32> listLinearAbs, out int maximum)
		{
			//Finished
			return MixBytes_Intern(listList, BitsPerSample, out listLinear, out listLinearAbs, out maximum);
		}
		/// <summary>
		/// MixBytes_Intern
		/// </summary>
		/// <param name="listList"></param>
		/// <param name="BitsPerSample"></param>
		/// <param name="listLinear"></param>
		/// <returns></returns>
		private static List<Byte> MixBytes_Intern(List<List<Byte>> listList, int BitsPerSample, out List<Int32> listLinear, out List<Int32> listLinearAbs, out int maximum)
		{

			//Set default value
			listLinear = new List<Int32>();
			listLinearAbs = new List<Int32>();
			maximum = 0;

			//Determine the maximum number of bytes for mixing
			int maxBytesCount = 0;
			foreach (List<Byte> l in listList)
			{
				if (l.Count > maxBytesCount)
				{
					maxBytesCount = l.Count;
				}
			}

			//If data exists
			if (listList.Count > 0 && maxBytesCount > 0)
			{

				//Depending on BitsPerSample
				switch (BitsPerSample)
				{
					//8
					case 8:
						return MixBytes_8Bit(listList, maxBytesCount, out listLinear, out listLinearAbs, out maximum);

					//16
					case 16:
						return MixBytes_16Bit(listList, maxBytesCount, out listLinear, out listLinearAbs, out maximum);
				}
			}

			//error
			return new List<Byte>();
		}
		/// <summary>
		/// MixBytes_16Bit
		/// </summary>
		/// <param name="listList"></param>
		/// <returns></returns>
		private static List<Byte> MixBytes_16Bit(List<List<Byte>> listList, int maxBytesCount, out List<Int32> listLinear, out List<Int32> listLinearAbs, out int maximum)
		{
			//Result
			maximum = 0;

			//Create an array of linear and byte values
			int linearCount = maxBytesCount / 2;
			Int32[] bytesLinear = new Int32[linearCount];
			Int32[] bytesLinearAbs = new Int32[linearCount];
			Byte[] bytesRaw = new Byte[maxBytesCount];

			//For every byte list
			for (int v = 0; v < listList.Count; v++)
			{
				//Convert to Array
				Byte[] bytes = listList[v].ToArray();

				//For every 16-bit value
				for (int i = 0, a = 0; i < linearCount; i++, a += 2)
				{
					//If values ​​for mixing exist
					if (i < bytes.Length && a < bytes.Length - 1)
					{
						//Determine value
						Int16 value16 = BitConverter.ToInt16(bytes, a);
						int value32 = bytesLinear[i] + value16;

						//Add value (catch overflows)
						if (value32 < Int16.MinValue)
						{
							value32 = Int16.MinValue;
						}
						else if (value32 > Int16.MaxValue)
						{
							value32 = Int16.MaxValue;
						}

						//Set values
						bytesLinear[i] = value32;
						bytesLinearAbs[i] = Math.Abs(value32);
						Int16 mixed16 = Convert.ToInt16(value32);
						Array.Copy(BitConverter.GetBytes(mixed16), 0, bytesRaw, a, 2);

						//Calculate maximum
						if (value32 > maximum)
						{
							maximum = value32;
						}
					}
					else
					{
						//Mute
					}
				}
			}

			//Out result
			listLinear = new List<int>(bytesLinear);
			listLinearAbs = new List<int>(bytesLinearAbs);

			//Finished
			return new List<Byte>(bytesRaw);
		}
		/// <summary>
		/// MixBytes_8Bit
		/// </summary>
		/// <param name="listList"></param>
		/// <param name="maxBytesCount"></param>
		/// <param name="listLinear"></param>
		/// <param name="listLinearAbs"></param>
		/// <param name="maximum"></param>
		/// <returns></returns>
		private static List<Byte> MixBytes_8Bit(List<List<Byte>> listList, int maxBytesCount, out List<Int32> listLinear, out List<Int32> listLinearAbs, out int maximum)
		{
			//Result
			maximum = 0;

			//Create an array of linear and byte values
			int linearCount = maxBytesCount;
			Int32[] bytesLinear = new Int32[linearCount];
			Byte[] bytesRaw = new Byte[maxBytesCount];

			//For every byte list
			for (int v = 0; v < listList.Count; v++)
			{
				//Convert to Array
				Byte[] bytes = listList[v].ToArray();

				//For every 8 bit value
				for (int i = 0; i < linearCount; i++)
				{
					//If values ​​for mixing exist
					if (i < bytes.Length)
					{
						//Determine value
						Byte value8 = bytes[i];
						int value32 = bytesLinear[i] + value8;

						//Add value (catch overflows)
						if (value32 < Byte.MinValue)
						{
							value32 = Byte.MinValue;
						}
						else if (value32 > Byte.MaxValue)
						{
							value32 = Byte.MaxValue;
						}

						//Set values
						bytesLinear[i] = value32;
						bytesRaw[i] = BitConverter.GetBytes(value32)[0];

						//Calculate maximum
						if (value32 > maximum)
						{
							maximum = value32;
						}
					}
					else
					{
						//Mute
					}
				}
			}

			//Out resultse
			listLinear = new List<int>(bytesLinear);
			listLinearAbs = new List<int>(bytesLinear);

			//Finished
			return new List<Byte>(bytesRaw);
		}
		/// <summary>
		/// SubsctractBytes_16Bit
		/// </summary>
		/// <param name="listList"></param>
		/// <param name="maxBytesCount"></param>
		/// <returns></returns>
		public static List<Byte> SubsctractBytes_16Bit(List<Byte> listSource, List<Byte> listToSubstract)
		{
			//Result
			List<Byte> list = new List<byte>(listSource.Count);

			//Create Array with Linear Values ​​(16Bit)
			int value16Count = listSource.Count / 2;
			List<Int16> list16Mixed = new List<Int16>(new Int16[value16Count]);

			//Convert to Array
			Byte[] bytesSource = listSource.ToArray();
			Byte[] bytesSubstract = listToSubstract.ToArray();

			//For every 16-bit value
			for (int i = 0, a = 0; i < value16Count; i++, a += 2)
			{
				//If values ​​exist
				if (i < bytesSource.Length && a < bytesSource.Length - 1)
				{
					//Determine values
					Int16 value16Source = BitConverter.ToInt16(bytesSource, a);
					Int16 value16Substract = BitConverter.ToInt16(bytesSubstract, a);
					int value32 = value16Source - value16Substract;

					//Add value (catch overflows)
					if (value32 < Int16.MinValue)
					{
						value32 = Int16.MinValue;
					}
					else if (value32 > Int16.MaxValue)
					{
						value32 = Int16.MaxValue;
					}

					//Add value
					list16Mixed[i] = Convert.ToInt16(value32);
				}
			}

			//For every value
			foreach (Int16 v16 in list16Mixed)
			{
				//Convert Integer to Bytes
				Byte[] bytes = BitConverter.GetBytes(v16);
				list.AddRange(bytes);

			}

			//Finished
			return list;
		}
	}
}
