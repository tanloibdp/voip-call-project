﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;

namespace WinSound
{
    unsafe public class Recorder
    {
        /// <summary>
        ///Constructor
        /// </summary>
        public Recorder()
        {
            delegateWaveInProc = new Win32.DelegateWaveInProc(waveInProc);
        }

        //Attributess
        private LockerClass Locker = new LockerClass();
        private LockerClass LockerCopy = new LockerClass();
        private IntPtr hWaveIn = IntPtr.Zero;
        private String WaveInDeviceName = "";
        private bool IsWaveInOpened = false;
        private bool IsWaveInStarted = false;
        private bool IsThreadRecordingRunning = false;
        private bool IsDataIncomming = false;
        private bool Stopped = false;
        private int SamplesPerSecond = 8000;
        private int BitsPerSample = 16;
        private int Channels = 1;
        private int BufferCount = 8;
        private int BufferSize = 1024;
        private Win32.WAVEHDR*[] WaveInHeaders;
        private Win32.WAVEHDR* CurrentRecordedHeader;
        private Win32.DelegateWaveInProc delegateWaveInProc;
        private System.Threading.Thread ThreadRecording;
        private System.Threading.AutoResetEvent AutoResetEventDataRecorded = new System.Threading.AutoResetEvent(false);

        //Delegates or events
        public delegate void DelegateStopped();
        public delegate void DelegateDataRecorded(Byte[] bytes);
        public event DelegateStopped RecordingStopped;
        public event DelegateDataRecorded DataRecorded;

        /// <summary>
        /// Started
        /// </summary>
        public bool Started
        {
            get
            {
                return IsWaveInStarted && IsWaveInOpened && IsThreadRecordingRunning;
            }
        }
        /// <summary>
        /// CreateWaveInHeaders
        /// </summary>
        /// <param name="count"></param>
        /// <param name="bufferSize"></param>
        /// <returns></returns>
        private bool CreateWaveInHeaders()
        {
            //Create buffer
            WaveInHeaders = new Win32.WAVEHDR*[BufferCount];
            int createdHeaders = 0;

            //For every buffer
            for (int i = 0; i < BufferCount; i++)
            {
                //Allocate headers
                WaveInHeaders[i] = (Win32.WAVEHDR*)Marshal.AllocHGlobal(sizeof(Win32.WAVEHDR));

                //Set headers
                WaveInHeaders[i]->dwLoops = 0;
                WaveInHeaders[i]->dwUser = IntPtr.Zero;
                WaveInHeaders[i]->lpNext = IntPtr.Zero;
                WaveInHeaders[i]->reserved = IntPtr.Zero;
                WaveInHeaders[i]->lpData = Marshal.AllocHGlobal(BufferSize);
                WaveInHeaders[i]->dwBufferLength = (uint)BufferSize;
                WaveInHeaders[i]->dwBytesRecorded = 0;
                WaveInHeaders[i]->dwFlags = 0;

                //If the buffer could be prepared
                Win32.MMRESULT hr = Win32.waveInPrepareHeader(hWaveIn, WaveInHeaders[i], sizeof(Win32.WAVEHDR));
                if (hr == Win32.MMRESULT.MMSYSERR_NOERROR)
                {
                    //Add first header for inclusion
                    if (i == 0)
                    {
                        hr = Win32.waveInAddBuffer(hWaveIn, WaveInHeaders[i], sizeof(Win32.WAVEHDR));
                    }
                    createdHeaders++;
                }
            }

            //Finished
            return (createdHeaders == BufferCount);
        }
        /// <summary>
        /// FreeWaveInHeaders
        /// </summary>
        private void FreeWaveInHeaders()
        {
            try
            {
                if (WaveInHeaders != null)
                {
                    for (int i = 0; i < WaveInHeaders.Length; i++)
                    {
                        //Release handle
                        Win32.MMRESULT hr = Win32.waveInUnprepareHeader(hWaveIn, WaveInHeaders[i], sizeof(Win32.WAVEHDR));

                        //Wait until Finished
                        int count = 0;
                        while (count <= 100 && (WaveInHeaders[i]->dwFlags & Win32.WaveHdrFlags.WHDR_INQUEUE) == Win32.WaveHdrFlags.WHDR_INQUEUE)
                        {
                            System.Threading.Thread.Sleep(20);
                            count++;
                        }

                        //When data is no longer in queue
                        if ((WaveInHeaders[i]->dwFlags & Win32.WaveHdrFlags.WHDR_INQUEUE) != Win32.WaveHdrFlags.WHDR_INQUEUE)
                        {
                            //Release data
                            if (WaveInHeaders[i]->lpData != IntPtr.Zero)
                            {
                                Marshal.FreeHGlobal(WaveInHeaders[i]->lpData);
                                WaveInHeaders[i]->lpData = IntPtr.Zero;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.Write(ex.Message);
            }
        }
        /// <summary>
        /// StartThreadRecording
        /// </summary>
        private void StartThreadRecording()
        {
            if (Started == false)
            {
                ThreadRecording = new System.Threading.Thread(new System.Threading.ThreadStart(OnThreadRecording));
                IsThreadRecordingRunning = true;
                ThreadRecording.Name = "Recording";
                ThreadRecording.Priority = System.Threading.ThreadPriority.Highest;
                ThreadRecording.Start();
            }
        }
        /// <summary>
        /// StartWaveIn
        /// </summary>
        /// <returns></returns>
        private bool OpenWaveIn()
        {
            if (hWaveIn == IntPtr.Zero)
            {
                //If not already open
                if (IsWaveInOpened == false)
                {
                    //Determine the format
                    Win32.WAVEFORMATEX waveFormatEx = new Win32.WAVEFORMATEX();
                    waveFormatEx.wFormatTag = (ushort)Win32.WaveFormatFlags.WAVE_FORMAT_PCM;
                    waveFormatEx.nChannels = (ushort)Channels;
                    waveFormatEx.nSamplesPerSec = (ushort)SamplesPerSecond;
                    waveFormatEx.wBitsPerSample = (ushort)BitsPerSample;
                    waveFormatEx.nBlockAlign = (ushort)((waveFormatEx.wBitsPerSample * waveFormatEx.nChannels) >> 3);
                    waveFormatEx.nAvgBytesPerSec = (uint)(waveFormatEx.nBlockAlign * waveFormatEx.nSamplesPerSec);

                    //Determine WaveIn device
                    int deviceId = WinSound.GetWaveInDeviceIdByName(WaveInDeviceName);
                    //WaveIn Gerät öffnen
                    Win32.MMRESULT hr = Win32.waveInOpen(ref hWaveIn, deviceId, ref waveFormatEx, delegateWaveInProc, 0, (int)Win32.WaveProcFlags.CALLBACK_FUNCTION);

                    //If not successful
                    if (hWaveIn == IntPtr.Zero)
                    {
                        IsWaveInOpened = false;
                        return false;
                    }

                    //Lock the handle
                    GCHandle.Alloc(hWaveIn, GCHandleType.Pinned);
                }
            }

            IsWaveInOpened = true;
            return true;
        }
        /// <summary>
        /// Start
        /// </summary>
        /// <param name="waveInDeviceName"></param>
        /// <param name="waveOutDeviceName"></param>
        /// <param name="samplesPerSecond"></param>
        /// <param name="bitsPerSample"></param>
        /// <param name="channels"></param>
        /// <returns></returns>
        public bool Start(string waveInDeviceName, int samplesPerSecond, int bitsPerSample, int channels, int bufferCount, int bufferSize)
        {
            try
            {
                lock (Locker)
                {
                    //If not already started
                    if (Started == false)
                    {

                        //Daten übernehmen
                        WaveInDeviceName = waveInDeviceName;
                        SamplesPerSecond = samplesPerSecond;
                        BitsPerSample = bitsPerSample;
                        Channels = channels;
                        BufferCount = bufferCount;
                        BufferSize = bufferSize;

                        //If WaveIn could be opened
                        if (OpenWaveIn())
                        {
                            //If all buffers could be generated
                            if (CreateWaveInHeaders())
                            {
                                //If the recording could be started
                                Win32.MMRESULT hr = Win32.waveInStart(hWaveIn);
                                if (hr == Win32.MMRESULT.MMSYSERR_NOERROR)
                                {
                                    IsWaveInStarted = true;
                                    //Start thread
                                    StartThreadRecording();
                                    Stopped = false;
                                    return true;
                                }
                                else
                                {
                                    //Error on startup
                                    return false;
                                }
                            }
                        }
                    }

                    //Repeater is already running
                    return false;
                }
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine(String.Format("Start | {0}", ex.Message));
                return false;
            }
        }
        /// <summary>
        /// Stop
        /// </summary>
        /// <returns></returns>
        public bool Stop()
        {
            try
            {
                lock (Locker)
                {
                    //When started
                    if (Started)
                    {
                        //As manually set to finish
                        Stopped = true;
                        IsThreadRecordingRunning = false;

                        //Close WaveIn
                        CloseWaveIn();

                        //Set of variables
                        AutoResetEventDataRecorded.Set();
                        return true;
                    }
                    return false;
                }
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine(String.Format("Stop | {0}", ex.Message));
                return false;
            }
        }
        /// <summary>
        /// CloseWaveIn
        /// </summary>
        /// <returns></returns>
        private void CloseWaveIn()
        {
            //Set buffer as finished
            Win32.MMRESULT hr = Win32.waveInStop(hWaveIn);

            int resetCount = 0;
            while (IsAnyWaveInHeaderInState(Win32.WaveHdrFlags.WHDR_INQUEUE) & resetCount < 20)
            {
                hr = Win32.waveInReset(hWaveIn);
                System.Threading.Thread.Sleep(50);
                resetCount++;
            }

            //Release header handles (before waveInClose)
            FreeWaveInHeaders();
            //Shut down
            hr = Win32.waveInClose(hWaveIn);
        }
        /// <summary>
        /// IsAnyWaveInHeaderInState
        /// </summary>
        /// <param name="state"></param>
        /// <returns></returns>
        private bool IsAnyWaveInHeaderInState(Win32.WaveHdrFlags state)
        {
            for (int i = 0; i < WaveInHeaders.Length; i++)
            {
                if ((WaveInHeaders[i]->dwFlags & state) == state)
                {
                    return true;
                }
            }
            return false;
        }
        /// <summary>
        /// waveInProc
        /// </summary>
        /// <param name="hWaveIn"></param>
        /// <param name="msg"></param>
        /// <param name="dwInstance"></param>
        /// <param name="wParam"></param>
        /// <param name="lParam"></param>
        private void waveInProc(IntPtr hWaveIn, Win32.WIM_Messages msg, IntPtr dwInstance, Win32.WAVEHDR* pWaveHdr, IntPtr lParam)
        {
            switch (msg)
            {
                //Open
                case Win32.WIM_Messages.OPEN:
                    break;

                //Data
                case Win32.WIM_Messages.DATA:
                    //Note incoming data
                    IsDataIncomming = true;
                    //Remember recorded buffer
                    CurrentRecordedHeader = pWaveHdr;
                    //Put event
                    AutoResetEventDataRecorded.Set();
                    break;

                //Close
                case Win32.WIM_Messages.CLOSE:
                    IsDataIncomming = false;
                    IsWaveInOpened = false;
                    AutoResetEventDataRecorded.Set();
                    this.hWaveIn = IntPtr.Zero;
                    break;
            }
        }
        /// <summary>
        /// OnThreadRecording
        /// </summary>
        private void OnThreadRecording()
        {
            while (Started && !Stopped)
            {
                //Wait for recording to finish
                AutoResetEventDataRecorded.WaitOne();

                try
                {
                    //When active
                    if (Started && !Stopped)
                    {
                        //If data exists
                        if (CurrentRecordedHeader->dwBytesRecorded > 0)
                        {
                            //When data is requested
                            if (DataRecorded != null && IsDataIncomming)
                            {
                                try
                                {
                                    //Copy data
                                    Byte[] bytes = new Byte[CurrentRecordedHeader->dwBytesRecorded];
                                    Marshal.Copy(CurrentRecordedHeader->lpData, bytes, 0, (int)CurrentRecordedHeader->dwBytesRecorded);

                                    //Send event
                                    DataRecorded(bytes);
                                }
                                catch (Exception ex)
                                {
                                    System.Diagnostics.Debug.WriteLine(String.Format("Recorder.cs | OnThreadRecording() | {0}", ex.Message));
                                }
                            }

                            //Continue recording
                            for (int i = 0; i < WaveInHeaders.Length; i++)
                            {
                                if ((WaveInHeaders[i]->dwFlags & Win32.WaveHdrFlags.WHDR_INQUEUE) == 0)
                                {
                                    Win32.MMRESULT hr = Win32.waveInAddBuffer(hWaveIn, WaveInHeaders[i], sizeof(Win32.WAVEHDR));
                                }
                            }

                        }
                    }

                    ////Recording
                    //StringBuilder rec = new StringBuilder();
                    //rec.AppendLine("");
                    //rec.AppendLine("Recording:");
                    //for (int i = 0; i < WaveInHeaders.Length; i++)
                    //{
                    //  rec.AppendLine(String.Format("{0} {1}", i, WinSound.FlagToString(WaveInHeaders[i].dwFlags)));

                    //}
                    //rec.AppendLine("");
                    //System.Diagnostics.Debug.WriteLine(rec.ToString());

                }
                catch (Exception ex)
                {
                    System.Diagnostics.Debug.WriteLine(ex.Message);
                }
            }


            lock (Locker)
            {
                //Set of variables
                IsWaveInStarted = false;
                IsThreadRecordingRunning = false;
            }

            //Send event
            if (RecordingStopped != null)
            {
                try
                {
                    RecordingStopped();
                }
                catch (Exception ex)
                {
                    System.Diagnostics.Debug.WriteLine(String.Format("Recording Stopped | {0}", ex.Message));
                }
            }
        }
    }
}
