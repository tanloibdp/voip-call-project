﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;

namespace WinSound
{
	/// <summary>
	/// QueueTimer
	/// </summary>
	public class QueueTimer
	{
		/// <summary>
		/// Constructor
		/// </summary>
		public QueueTimer()
		{
			_DelegateTimerProc = new global::WinSound.Win32.DelegateTimerProc(OnTimer);
		}

		//Attributes
		private bool _IsRunning = false;
		private uint _Milliseconds = 20;
		private IntPtr _HandleTimer = IntPtr.Zero;
		private GCHandle _GCHandleTimer;
		private uint _ResolutionInMilliseconds = 0;
		private IntPtr _HandleTimerQueue;
		private GCHandle _GCHandleTimerQueue;

		//Delegates or events
		private global::WinSound.Win32.DelegateTimerProc _DelegateTimerProc;
		public delegate void DelegateTimerTick();
		public event DelegateTimerTick TimerTick;

		/// <summary>
		/// IsRunning
		/// </summary>
		/// <returns></returns>
		public bool IsRunning
		{
			get
			{
				return _IsRunning;
			}
		}
		/// <summary>
		/// Milliseconds
		/// </summary>
		public uint Milliseconds
		{
			get
			{
				return _Milliseconds;
			}
		}
		/// <summary>
		/// ResolutionInMilliseconds
		/// </summary>
		public uint ResolutionInMilliseconds
		{
			get
			{
				return _ResolutionInMilliseconds;
			}
		}
		/// <summary>
		/// SetBestResolution
		/// </summary>
		public static void SetBestResolution()
		{
			//QueueTimer Auflösung ermitteln
			global::WinSound.Win32.TimeCaps tc = new global::WinSound.Win32.TimeCaps();
			global::WinSound.Win32.TimeGetDevCaps(ref tc, (uint)Marshal.SizeOf(typeof(global::WinSound.Win32.TimeCaps)));
			uint resolution = Math.Max(tc.wPeriodMin, 0);

			//QueueTimer Resolution setzen
			global::WinSound.Win32.TimeBeginPeriod(resolution);
		}
		/// <summary>
		/// ResetResolution
		/// </summary>
		public static void ResetResolution()
		{
			//QueueTimer Auflösung ermitteln
			global::WinSound.Win32.TimeCaps tc = new global::WinSound.Win32.TimeCaps();
			global::WinSound.Win32.TimeGetDevCaps(ref tc, (uint)Marshal.SizeOf(typeof(global::WinSound.Win32.TimeCaps)));
			uint resolution = Math.Max(tc.wPeriodMin, 0);

			//QueueTimer Resolution deaktivieren
			global::WinSound.Win32.TimeBeginPeriod(resolution);
		}
		/// <summary>
		/// Start
		/// </summary>
		/// <param name="milliseconds"></param>
		/// <param name="dueTimeInMilliseconds"></param>
		public void Start(uint milliseconds, uint dueTimeInMilliseconds)
		{
			//take values
			_Milliseconds = milliseconds;

			//QueueTimer Auflösung ermitteln
			global::WinSound.Win32.TimeCaps tc = new global::WinSound.Win32.TimeCaps();
			global::WinSound.Win32.TimeGetDevCaps(ref tc, (uint)Marshal.SizeOf(typeof(global::WinSound.Win32.TimeCaps)));
			_ResolutionInMilliseconds = Math.Max(tc.wPeriodMin, 0);

			//QueueTimer Resolution setzen
			global::WinSound.Win32.TimeBeginPeriod(_ResolutionInMilliseconds);

			//QueueTimer Queue erstellen
			_HandleTimerQueue = global::WinSound.Win32.CreateTimerQueue();
			_GCHandleTimerQueue = GCHandle.Alloc(_HandleTimerQueue);

			//Versuche QueueTimer zu starten
			bool resultCreate = global::WinSound.Win32.CreateTimerQueueTimer(out _HandleTimer, _HandleTimerQueue, _DelegateTimerProc, IntPtr.Zero, dueTimeInMilliseconds, _Milliseconds, global::WinSound.Win32.WT_EXECUTEINTIMERTHREAD);
			if (resultCreate)
			{
				//Handle im Speicher halten
				_GCHandleTimer = GCHandle.Alloc(_HandleTimer, GCHandleType.Pinned);
				//QueueTimer ist gestartet
				_IsRunning = true;
			}
		}
		/// <summary>
		/// Stop
		/// </summary>
		public void Stop()
		{
			if (_HandleTimer != IntPtr.Zero)
			{
				//QueueEnd timer
				global:: WinSound.Win32.DeleteTimerQueueTimer(IntPtr.Zero, _HandleTimer, IntPtr.Zero);
				//QueueTimer Resolution break up
				global::WinSound.Win32.TimeEndPeriod(_ResolutionInMilliseconds);

				//QueueTimer Queue löschen
				if (_HandleTimerQueue != IntPtr.Zero)
				{
					global::WinSound.Win32.DeleteTimerQueue(_HandleTimerQueue);
				}

				//Release handles
				if (_GCHandleTimer.IsAllocated)
				{
					_GCHandleTimer.Free();
				}
				if (_GCHandleTimerQueue.IsAllocated)
				{
					_GCHandleTimerQueue.Free();
				}

				//Set of variables
				_HandleTimer = IntPtr.Zero;
				_HandleTimerQueue = IntPtr.Zero;
				_IsRunning = false;
			}
		}
		/// <summary>
		/// OnTimer
		/// </summary>
		/// <param name="lpParameter"></param>
		/// <param name="TimerOrWaitFired"></param>
		private void OnTimer(IntPtr lpParameter, bool TimerOrWaitFired)
		{
			if (TimerTick != null)
			{
				TimerTick();
			}
		}
	}

	/// <summary>
	/// QueueTimer
	/// </summary>
	public class EventTimer
	{
		/// <summary>
		/// Constructor
		/// </summary>
		public EventTimer()
		{
			_DelegateTimeEvent = new global::WinSound.Win32.TimerEventHandler(OnTimer);
		}

		//Attributes
		private bool _IsRunning = false;
		private uint _Milliseconds = 20;
		private UInt32 _TimerId = 0;
		private GCHandle _GCHandleTimer;
		private UInt32 _UserData = 0;
		private uint _ResolutionInMilliseconds = 0;

		//Delegates or events
		private global::WinSound.Win32.TimerEventHandler _DelegateTimeEvent;
		public delegate void DelegateTimerTick();
		public event DelegateTimerTick TimerTick;

		/// <summary>
		/// IsRunning
		/// </summary>
		/// <returns></returns>
		public bool IsRunning
		{
			get
			{
				return _IsRunning;
			}
		}
		/// <summary>
		/// Milliseconds
		/// </summary>
		public uint Milliseconds
		{
			get
			{
				return _Milliseconds;
			}
		}
		/// <summary>
		/// ResolutionInMilliseconds
		/// </summary>
		public uint ResolutionInMilliseconds
		{
			get
			{
				return _ResolutionInMilliseconds;
			}
		}
		/// <summary>
		/// SetBestResolution
		/// </summary>
		public static void SetBestResolution()
		{
			//QueueTimer Auflösung ermitteln
			global::WinSound.Win32.TimeCaps tc = new global::WinSound.Win32.TimeCaps();
			global::WinSound.Win32.TimeGetDevCaps(ref tc, (uint)Marshal.SizeOf(typeof(global::WinSound.Win32.TimeCaps)));
			uint resolution = Math.Max(tc.wPeriodMin, 0);

			//QueueTimer Resolution setzen
			global::WinSound.Win32.TimeBeginPeriod(resolution);
		}
		/// <summary>
		/// ResetResolution
		/// </summary>
		public static void ResetResolution()
		{
			//QueueTimer Auflösung ermitteln
			global::WinSound.Win32.TimeCaps tc = new global::WinSound.Win32.TimeCaps();
			global::WinSound.Win32.TimeGetDevCaps(ref tc, (uint)Marshal.SizeOf(typeof(global::WinSound.Win32.TimeCaps)));
			uint resolution = Math.Max(tc.wPeriodMin, 0);

			//QueueTimer Resolution deaktivieren
			global::WinSound.Win32.TimeEndPeriod(resolution);
		}
		/// <summary>
		/// Start
		/// </summary>
		/// <param name="milliseconds"></param>
		/// <param name="dueTimeInMilliseconds"></param>
		public void Start(uint milliseconds, uint dueTimeInMilliseconds)
		{
			//take values
			_Milliseconds = milliseconds;

			//Timer Auflösung ermitteln
			global::WinSound.Win32.TimeCaps tc = new global::WinSound.Win32.TimeCaps();
			global::WinSound.Win32.TimeGetDevCaps(ref tc, (uint)Marshal.SizeOf(typeof(global::WinSound.Win32.TimeCaps)));
			_ResolutionInMilliseconds = Math.Max(tc.wPeriodMin, 0);

			//Timer Resolution setzen
			global::WinSound.Win32.TimeBeginPeriod(_ResolutionInMilliseconds);

			//Versuche EventTimer zu starten
			_TimerId = global::WinSound.Win32.TimeSetEvent(_Milliseconds, _ResolutionInMilliseconds, _DelegateTimeEvent, ref _UserData, (UInt32)Win32.TIME_PERIODIC);
			if (_TimerId > 0)
			{
				//Handle im Speicher halten
				_GCHandleTimer = GCHandle.Alloc(_TimerId, GCHandleType.Pinned);
				//QueueTimer ist gestartet
				_IsRunning = true;
			}
		}
		/// <summary>
		/// Stop
		/// </summary>
		public void Stop()
		{
			if (_TimerId > 0)
			{
				//End timer
				global:: WinSound.Win32.TimeKillEvent(_TimerId);
				//Timer Resolution break up
				global::WinSound.Win32.TimeEndPeriod(_ResolutionInMilliseconds);

				//Release handles
				if (_GCHandleTimer.IsAllocated)
				{
					_GCHandleTimer.Free();
				}

				//Set of variables
				_TimerId = 0;
				_IsRunning = false;
			}
		}
		/// <summary>
		/// OnTimer
		/// </summary>
		/// <param name="lpParameter"></param>
		/// <param name="TimerOrWaitFired"></param>
		private void OnTimer(UInt32 id, UInt32 msg, ref UInt32 userCtx, UInt32 rsv1, UInt32 rsv2)
		{
			if (TimerTick != null)
			{
				TimerTick();
			}
		}
	}
	/// <summary>
	/// Stopwatch
	/// </summary>
	public class Stopwatch
	{
		/// <summary>
		/// Stopwatch
		/// </summary>
		public Stopwatch()
		{
			//Check
			if (Win32.QueryPerformanceFrequency(out _Frequency) == false)
			{
				throw new Exception("High Performance counter not supported");
			}
		}

		//Attributes
		private long _StartTime = 0;
		private long _DurationTime = 0;
		private long _Frequency;

		/// <summary>
		/// Start
		/// </summary>
		public void Start()
		{
			Win32.QueryPerformanceCounter(out _StartTime);
			_DurationTime = _StartTime;
		}
		/// <summary>
		/// ElapsedMilliseconds
		/// </summary>
		public double ElapsedMilliseconds
		{
			get
			{
				Win32.QueryPerformanceCounter(out _DurationTime);
				return (double)(_DurationTime - _StartTime) / (double)_Frequency * 1000;
			}
		}
	}
}
