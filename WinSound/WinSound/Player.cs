﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;

namespace WinSound
{
    /// <summary>
    /// Player
    /// </summary>
    unsafe public class Player
    {
        /// <summary>
        /// Constructor
        /// </summary>
        public Player()
        {

            delegateWaveOutProc = new Win32.DelegateWaveOutProc(waveOutProc);
        }

        //Attributes
        private LockerClass Locker = new LockerClass();
        private LockerClass LockerCopy = new LockerClass();
        private IntPtr hWaveOut = IntPtr.Zero;
        private String WaveOutDeviceName = "";
        private bool IsWaveOutOpened = false;
        private bool IsThreadPlayWaveOutRunning = false;
        private bool IsClosed = false;
        private bool IsPaused = false;
        private bool IsStarted = false;
        private bool IsBlocking = false;
        private int SamplesPerSecond = 8000;
        private int BitsPerSample = 16;
        private int Channels = 1;
        private int BufferCount = 8;
        private int BufferLength = 1024;
        private Win32.WAVEHDR*[] WaveOutHeaders;
        private Win32.DelegateWaveOutProc delegateWaveOutProc;
        private System.Threading.Thread ThreadPlayWaveOut;
        private System.Threading.AutoResetEvent AutoResetEventDataPlayed = new System.Threading.AutoResetEvent(false);

        //Delegates or events
        public delegate void DelegateStopped();
        public event DelegateStopped PlayerClosed;
        public event DelegateStopped PlayerStopped;

        /// <summary>
        /// Paused
        /// </summary>
        public bool Paused
        {
            get
            {
                return IsPaused;
            }
        }
        /// <summary>
        /// Opened
        /// </summary>
        public bool Opened
        {
            get
            {
                return IsWaveOutOpened & IsClosed == false;
            }
        }
        /// <summary>
        /// Playing
        /// </summary>
        public bool Playing
        {
            get
            {
                if (Opened && IsStarted)
                {
                    foreach (Win32.WAVEHDR* pHeader in WaveOutHeaders)
                    {
                        if (IsHeaderInqueue(*pHeader))
                        {
                            return true;
                        }
                    }
                }
                return false;
            }
        }
        /// <summary>
        /// CreateWaveOutHeaders
        /// </summary>
        /// <returns></returns>
        private bool CreateWaveOutHeaders()
        {
            //Create buffer
            WaveOutHeaders = new Win32.WAVEHDR*[BufferCount];
            int createdHeaders = 0;

            //For every buffer
            for (int i = 0; i < BufferCount; i++)
            {
                //Allocate headers
                WaveOutHeaders[i] = (Win32.WAVEHDR*)Marshal.AllocHGlobal(sizeof(Win32.WAVEHDR));

                //Set headers
                WaveOutHeaders[i]->dwLoops = 0;
                WaveOutHeaders[i]->dwUser = IntPtr.Zero;
                WaveOutHeaders[i]->lpNext = IntPtr.Zero;
                WaveOutHeaders[i]->reserved = IntPtr.Zero;
                WaveOutHeaders[i]->lpData = Marshal.AllocHGlobal(BufferLength);
                WaveOutHeaders[i]->dwBufferLength = (uint)BufferLength;
                WaveOutHeaders[i]->dwBytesRecorded = 0;
                WaveOutHeaders[i]->dwFlags = 0;
  
                //If the buffer could be prepared
                Win32.MMRESULT hr = Win32.waveOutPrepareHeader(hWaveOut, WaveOutHeaders[i], sizeof(Win32.WAVEHDR));
                if (hr == Win32.MMRESULT.MMSYSERR_NOERROR)
                {
                    createdHeaders++;
                }
            }

            //Finished
            return (createdHeaders == BufferCount);
        }
        /// <summary>
        /// FreeWaveInHeaders
        /// </summary>
        private void FreeWaveOutHeaders()
        {
            try
            {
                if (WaveOutHeaders != null)
                {
                    for (int i = 0; i < WaveOutHeaders.Length; i++)
                    {
                        //Release handles
                        Win32.MMRESULT hr = Win32.waveOutUnprepareHeader(hWaveOut, WaveOutHeaders[i], sizeof(Win32.WAVEHDR));

                        //Wait for Finished to play
                        int count = 0;
                        while(count <= 100 && (WaveOutHeaders[i]->dwFlags & Win32.WaveHdrFlags.WHDR_INQUEUE) == Win32.WaveHdrFlags.WHDR_INQUEUE)
                        {
                            System.Threading.Thread.Sleep(20);
                            count++;
                        }

                        //When playing data  
                        if ((WaveOutHeaders[i]->dwFlags & Win32.WaveHdrFlags.WHDR_INQUEUE) != Win32.WaveHdrFlags.WHDR_INQUEUE)
                        {
                            //Release data
                            if (WaveOutHeaders[i]->lpData != IntPtr.Zero)
                            {
                                Marshal.FreeHGlobal(WaveOutHeaders[i]->lpData);
                                WaveOutHeaders[i]->lpData = IntPtr.Zero;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.Write(ex.Message);
            }
        }
        /// <summary>
        /// StartThreadRecording
        /// </summary>
        private void StartThreadPlayWaveOut()
        {
            if (IsThreadPlayWaveOutRunning == false)
            {
                ThreadPlayWaveOut = new System.Threading.Thread(new System.Threading.ThreadStart(OnThreadPlayWaveOut));
                IsThreadPlayWaveOutRunning = true;
                ThreadPlayWaveOut.Name = "PlayWaveOut";
                ThreadPlayWaveOut.Priority = System.Threading.ThreadPriority.Highest;
                ThreadPlayWaveOut.Start();
            }
        }
        /// <summary>
        /// PlayBytes. Bytes in gleich grosse Stücke teilen und einzeln Play
        /// </summary>
        /// <param name="bytes"></param>
        /// <returns></returns>
        private bool PlayBytes(Byte[] bytes)
        {
            if (bytes.Length > 0)
            {
                //Size of byte pieces 
                int byteSize = bytes.Length / BufferCount;
                int currentPos = 0;

                //For any buffer
                for (int count = 0; count < BufferCount; count++)
                {
                    //Determine the next free buffer
                    int index = GetNextFreeWaveOutHeaderIndex();
                    if (index != -1)
                    {
                        try
                        {
                            //Copy section
                            Byte[] partByte = new Byte[byteSize];
                            Array.Copy(bytes, currentPos, partByte, 0, byteSize);
                            currentPos += byteSize;

                            //If different data size
                            if (WaveOutHeaders[index]->dwBufferLength != partByte.Length)
                            {
                                //Create new data store
                                Marshal.FreeHGlobal(WaveOutHeaders[index]->lpData);
                                WaveOutHeaders[index]->lpData = Marshal.AllocHGlobal(partByte.Length);
                                WaveOutHeaders[index]->dwBufferLength = (uint)partByte.Length;
                            }

                            //Copy data
                            WaveOutHeaders[index]->dwUser = (IntPtr)index;
                            Marshal.Copy(partByte, 0, WaveOutHeaders[index]->lpData, partByte.Length);
                        }
                        catch (Exception ex)
                        {
                            //Error while copying
                            System.Diagnostics.Debug.WriteLine(String.Format("CopyBytesToFreeWaveOutHeaders() | {0}", ex.Message));
                            AutoResetEventDataPlayed.Set();
                            return false;
                        }

                        //If still open
                        if (hWaveOut != null)
                        {
                            //Play
                            Win32.MMRESULT hr = Win32.waveOutWrite(hWaveOut, WaveOutHeaders[index], sizeof(Win32.WAVEHDR));
                            if (hr != Win32.MMRESULT.MMSYSERR_NOERROR)
                            {
                                //Error playing
                                AutoResetEventDataPlayed.Set();
                                return false;
                            }
                        }
                        else
                        {
                            //WaveOut invalid
                            return false;
                        }
                    }
                    else
                    {
                        //Not enough free buffer available
                        return false;
                    }
                }
                return true;
            }
            //No data available
            return false;
        }
        /// <summary>
        /// OpenWaveOuz
        /// </summary>
        /// <returns></returns>
        private bool OpenWaveOut()
        {
            if (hWaveOut == IntPtr.Zero)
            {
                //If not already open
                if (IsWaveOutOpened == false)
                {
                    //Format bestimmen
                    Win32.WAVEFORMATEX waveFormatEx = new Win32.WAVEFORMATEX();
                    waveFormatEx.wFormatTag = (ushort)Win32.WaveFormatFlags.WAVE_FORMAT_PCM;
                    waveFormatEx.nChannels = (ushort)Channels;
                    waveFormatEx.nSamplesPerSec = (ushort)SamplesPerSecond;
                    waveFormatEx.wBitsPerSample = (ushort)BitsPerSample;
                    waveFormatEx.nBlockAlign = (ushort)((waveFormatEx.wBitsPerSample * waveFormatEx.nChannels) >> 3);
                    waveFormatEx.nAvgBytesPerSec = (uint)(waveFormatEx.nBlockAlign * waveFormatEx.nSamplesPerSec);

                    //WaveOut Gerät ermitteln
                    int deviceId = WinSound.GetWaveOutDeviceIdByName(WaveOutDeviceName);
                    //WaveIn Gerät öffnen
                    Win32.MMRESULT hr = Win32.waveOutOpen(ref hWaveOut, deviceId, ref waveFormatEx, delegateWaveOutProc, 0, (int)Win32.WaveProcFlags.CALLBACK_FUNCTION);

                    //Wenn nicht erfolgreich
                    if (hr != Win32.MMRESULT.MMSYSERR_NOERROR)
                    {
                        IsWaveOutOpened = false;
                        return false;
                    }

                    //Handle sperren
                    GCHandle.Alloc(hWaveOut, GCHandleType.Pinned);
                }
            }

            IsWaveOutOpened = true;
            return true;
        }
        /// <summary>
        ///Open
        /// </summary>
        /// <param name="waveInDeviceName"></param>
        /// <param name="waveOutDeviceName"></param>
        /// <param name="samplesPerSecond"></param>
        /// <param name="bitsPerSample"></param>
        /// <param name="channels"></param>
        /// <returns></returns>
        public bool Open(string waveOutDeviceName, int samplesPerSecond, int bitsPerSample, int channels, int bufferCount)
        {
            try
            {
                lock (Locker)
                {
                    //Wenn nicht schon geöffnet
                    if (Opened == false)
                    {

                        //Daten übernehmen
                        WaveOutDeviceName = waveOutDeviceName;
                        SamplesPerSecond = samplesPerSecond;
                        BitsPerSample = bitsPerSample;
                        Channels = channels;
                        BufferCount = Math.Max(bufferCount, 1);

                        //Wenn WaveOut geöffnet werden konnte
                        if (OpenWaveOut())
                        {
                            //Wenn alle Buffer erzeugt werden konnten
                            if (CreateWaveOutHeaders())
                            {
                                //Thread starten
                                StartThreadPlayWaveOut();
                                IsClosed = false;
                                return true;
                            }
                        }
                    }

                    //Schon geöffnet
                    return false;
                }
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine(String.Format("Start | {0}", ex.Message));
                return false;
            }
        }
        /// <summary>
        /// PlayData
        /// </summary>
        /// <param name="datas"></param>
        /// <param name="isBlocking"></param>
        /// <returns></returns>
        public bool PlayData(Byte[] datas, bool isBlocking)
        {
            try
            {
                if (Opened)
                {
                    int index = GetNextFreeWaveOutHeaderIndex();
                    if (index != -1)
                    {
                        //take values
                        this.IsBlocking = isBlocking;

                        //If different data size
                        if (WaveOutHeaders[index]->dwBufferLength != datas.Length)
                        {
                            //Create new data store
                            Marshal.FreeHGlobal(WaveOutHeaders[index]->lpData);
                            WaveOutHeaders[index]->lpData = Marshal.AllocHGlobal(datas.Length);
                            WaveOutHeaders[index]->dwBufferLength = (uint)datas.Length;
                        }

                        //Copy data
                        WaveOutHeaders[index]->dwBufferLength = (uint)datas.Length;
                        WaveOutHeaders[index]->dwUser = (IntPtr)index;
                        Marshal.Copy(datas, 0, WaveOutHeaders[index]->lpData, datas.Length);

                        //Play
                        this.IsStarted = true;
                        Win32.MMRESULT hr = Win32.waveOutWrite(hWaveOut, WaveOutHeaders[index], sizeof(Win32.WAVEHDR));
                        if (hr == Win32.MMRESULT.MMSYSERR_NOERROR)
                        {
                            //When blocking
                            if (isBlocking)
                            {
                                AutoResetEventDataPlayed.WaitOne();
                                AutoResetEventDataPlayed.Set();
                            }
                            return true;
                        }
                        else
                        {
                            //Error playing
                            AutoResetEventDataPlayed.Set();
                            return false;
                        }
                    }
                    else
                    {
                        //No free output buffer available
                        System.Diagnostics.Debug.WriteLine(String.Format("No free WaveOut Buffer found | {0}", DateTime.Now.ToLongTimeString()));
                        return false;
                    }
                }
                else
                {
                    //Not open
                    return false;
                }

            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine(String.Format("PlayData | {0}", ex.Message));
                return false;
            }
        }
        /// <summary>
        /// PlayFile (Wave Files)
        /// </summary>
        /// <param name="fileName"></param>
        /// <returns></returns>
        public bool PlayFile(string fileName, string waveOutDeviceName)
        {
            lock (Locker)
            {
                try
                {
                    //Read WaveFile
                    WaveFileHeader header = WaveFile.Read(fileName);

                    //If data exists
                    if (header.Payload.Length > 0)
                    {
                        //When open
                        if (Open(waveOutDeviceName, (int)header.SamplesPerSecond, (int)header.BitsPerSample, (int)header.Channels, 8))
                        {
                            int index = GetNextFreeWaveOutHeaderIndex();
                            if (index != -1)
                            {
                                //Bytes Partially in output buffer Play
                                this.IsStarted = true;
                                return PlayBytes(header.Payload);
                            }
                            else
                            {
                                //No free output buffer available
                                AutoResetEventDataPlayed.Set();
                                return false;
                            }
                        }
                        else
                        {
                            //Not open
                            AutoResetEventDataPlayed.Set();
                            return false;
                        }
                    }
                    else
                    {
                        //Fehlerhafte Datei
                        AutoResetEventDataPlayed.Set();
                        return false;
                    }
                }
                catch (Exception ex)
                {
                    System.Diagnostics.Debug.WriteLine(String.Format("PlayFile | {0}", ex.Message));
                    AutoResetEventDataPlayed.Set();
                    return false;
                }
            }
        }
        /// <summary>
        /// Close
        /// </summary>
        /// <returns></returns>
        public bool Close()
        {
            try
            {
                lock (Locker)
                {
                    //When open
                    if (Opened)
                    {
                        //As manually set to finish
                        IsClosed = true;

                        //Wait until all data finishes playing
                        int count = 0;
                        while (Win32.waveOutReset(hWaveOut) != Win32.MMRESULT.MMSYSERR_NOERROR && count <= 100)
                        {
                            System.Threading.Thread.Sleep(50);
                            count++;
                        }

                        //Header and release data
                        FreeWaveOutHeaders();

                        //Wait until all data finishes playing
                        count = 0;
                        while (Win32.waveOutClose(hWaveOut) != Win32.MMRESULT.MMSYSERR_NOERROR && count <= 100)
                        {
                            System.Threading.Thread.Sleep(50);
                            count++;
                        }

                        //Set of variables
                        IsWaveOutOpened = false;
                        AutoResetEventDataPlayed.Set();
                        return true;
                    }
                    return false;
                }
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine(String.Format("Close | {0}", ex.Message));
                return false;
            }
        }
        /// <summary>
        /// StartPause
        /// </summary>
        /// <returns></returns>
        public bool StartPause()
        {
            try
            {
                lock (Locker)
                {
                    //When open
                    if (Opened)
                    {
                        //If not already paused
                        if (IsPaused == false)
                        {
                            //Pause
                            Win32.MMRESULT hr = Win32.waveOutPause(hWaveOut);
                            if (hr == Win32.MMRESULT.MMSYSERR_NOERROR)
                            {
                                //to save
                                IsPaused = true;
                                AutoResetEventDataPlayed.Set();
                                return true;
                            }
                        }
                    }
                    return false;
                }
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine(String.Format("StartPause | {0}", ex.Message));
                return false;
            }
        }
        /// <summary>
        /// EndPause
        /// </summary>
        /// <returns></returns>
        public bool EndPause()
        {
            try
            {
                lock (Locker)
                {
                    //When open
                    if (Opened)
                    {
                        //When paused
                        if (IsPaused)
                        {
                            //Pause
                            Win32.MMRESULT hr = Win32.waveOutRestart(hWaveOut);
                            if (hr == Win32.MMRESULT.MMSYSERR_NOERROR)
                            {
                                //to save
                                IsPaused = false;
                                AutoResetEventDataPlayed.Set();
                                return true;
                            }
                        }
                    }
                    return false;
                }
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine(String.Format("EndPause | {0}", ex.Message));
                return false;
            }
        }
        /// <summary>
        /// GetNextFreeWaveOutHeaderIndex
        /// </summary>
        /// <returns></returns>
        private int GetNextFreeWaveOutHeaderIndex()
        {
            for (int i = 0; i < WaveOutHeaders.Length; i++)
            {
                if (IsHeaderPrepared(*WaveOutHeaders[i]) && !IsHeaderInqueue(*WaveOutHeaders[i]))
                {
                    return i;
                }
            }
            return -1;
        }
        /// <summary>
        /// IsHeaderPrepared
        /// </summary>
        /// <param name="flags"></param>
        /// <returns></returns>
        private bool IsHeaderPrepared(Win32.WAVEHDR header)
        {
            return (header.dwFlags & Win32.WaveHdrFlags.WHDR_PREPARED) > 0;
        }
        /// <summary>
        /// IsHeaderInqueue
        /// </summary>
        /// <param name="flags"></param>
        /// <returns></returns>
        private bool IsHeaderInqueue(Win32.WAVEHDR header)
        {
            return (header.dwFlags & Win32.WaveHdrFlags.WHDR_INQUEUE) > 0;
        }
        /// <summary>
        /// waveOutProc
        /// </summary>
        /// <param name="hWaveOut"></param>
        /// <param name="msg"></param>
        /// <param name="dwInstance"></param>
        /// <param name="wParam"></param>
        /// <param name="lParam"></param>
        private void waveOutProc(IntPtr hWaveOut, Win32.WOM_Messages msg, IntPtr dwInstance, Win32.WAVEHDR* pWaveHeader, IntPtr lParam)
        {
            try
            {
                switch (msg)
                {
                    //Open
                    case Win32.WOM_Messages.OPEN:
                        break;

                    //Done
                    case Win32.WOM_Messages.DONE:
                        //Note the data arrive
                        IsStarted = true;
                        AutoResetEventDataPlayed.Set();
                        break;

                    //Close
                    case Win32.WOM_Messages.CLOSE:
                        IsStarted = false;
                        IsWaveOutOpened = false;
                        IsPaused = false;
                        IsClosed = true;
                        AutoResetEventDataPlayed.Set();
                        this.hWaveOut = IntPtr.Zero;
                        break;
                }
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine(String.Format("Player.cs | waveOutProc() | {0}", ex.Message));
                AutoResetEventDataPlayed.Set();
            }
        }
        /// <summary>
        /// OnThreadRecording
        /// </summary>
        private void OnThreadPlayWaveOut()
        {
            while (Opened && !IsClosed)
            {
                //Wait for recording to finish
                AutoResetEventDataPlayed.WaitOne();

                lock (Locker)
                {
                    if (Opened && !IsClosed)
                    {
                        //Set of variables
                        IsThreadPlayWaveOutRunning = true;

                        //When no data is playing anymore
                        if (!Playing)
                        {
                            //When playing data became
                            if (IsStarted)
                            {
                                IsStarted = false;
                                //Event abSend
                                if (PlayerStopped != null)
                                {
                                    try
                                    {
                                        PlayerStopped();
                                    }
                                    catch (Exception ex)
                                    {
                                        System.Diagnostics.Debug.WriteLine(String.Format("Player Stopped | {0}", ex.Message));
                                    }
                                    finally
                                    {
                                        AutoResetEventDataPlayed.Set();
                                    }
                                }
                            }
                        }
                    }
                }

                //When blocking
                if (IsBlocking)
                {
                    AutoResetEventDataPlayed.Set();
                }
            }

            lock (Locker)
            {
                //Set of variables
                IsThreadPlayWaveOutRunning = false;
            }

            //Event out of broadcast
            if (PlayerClosed != null)
            {
                try
                {
                    PlayerClosed();
                }
                catch (Exception ex)
                {
                    System.Diagnostics.Debug.WriteLine(String.Format("Player Closed | {0}", ex.Message));
                }
            }
        }
    }
}
