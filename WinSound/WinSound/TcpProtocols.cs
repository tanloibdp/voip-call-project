﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WinSound
{
	/// <summary>
	/// ProtocolTypes
	/// </summary>
	public enum ProtocolTypes
	{
		LH
	}
	/// <summary>
	/// Protocol
	/// </summary>
	public class Protocol
	{
		/// <summary>
		/// Constructor
		/// </summary>
		/// <param name="type"></param>
		public Protocol(ProtocolTypes type, Encoding encoding)
		{
			this.m_ProtocolType = type;
			this.m_Encoding = encoding;
		}

		//Attributes
		private List<Byte> m_DataBuffer = new List<byte>();
		private const int m_MaxBufferLength = 10000;
		private ProtocolTypes m_ProtocolType = ProtocolTypes.LH;
		private Encoding m_Encoding = Encoding.Default;
		public Object m_LockerReceive = new object();

		//Delegates or events
		public delegate void DelegateDataComplete(Object sender, Byte[] data);
		public delegate void DelegateExceptionAppeared(Object sender, Exception ex);
		public event DelegateDataComplete DataComplete;
		public event DelegateExceptionAppeared ExceptionAppeared;


		/// <summary>
		/// ToBytes
		/// </summary>
		/// <param name="data"></param>
		/// <returns></returns>
		public Byte[] ToBytes(Byte[] data)
		{
			try
			{
				//Bytes length
				Byte[] bytesLength = BitConverter.GetBytes(data.Length);

				//Sum up everything
				Byte[] allBytes = new Byte[bytesLength.Length + data.Length];
				Array.Copy(bytesLength, allBytes, bytesLength.Length);
				Array.Copy(data, 0, allBytes, bytesLength.Length, data.Length);

				//Finished
				return allBytes;
			}
			catch (Exception ex)
			{
				ExceptionAppeared(null, ex);
			}

            //error
            return data;
		}
		/// <summary>
		/// Receive_LH_STX_ETX
		/// </summary>
		/// <param name="data"></param>
		public void Receive_LH(Object sender, Byte[] data)
		{
			lock (m_LockerReceive)
			{
				try
				{
					//Append data to buffer
					m_DataBuffer.AddRange(data);

					//Prevent buffer overflow
					if (m_DataBuffer.Count > m_MaxBufferLength)
					{
						m_DataBuffer.Clear();
					}

					//Read out bytes
					Byte[] bytes = m_DataBuffer.Take(4).ToArray();
					//Determine length
					int length = (int)BitConverter.ToInt32(bytes.ToArray(), 0);

					//Ensure maximum length
					if (length > m_MaxBufferLength)
					{
						m_DataBuffer.Clear();
					}

					//As long as data is available
					while (m_DataBuffer.Count >= length + 4)
					{
						//Extract data
						Byte[] message = m_DataBuffer.Skip(4).Take(length).ToArray();

						//Notification of complete data
						if (DataComplete != null)
						{
							DataComplete(sender, message);
						}
						//Remove data from buffer
						m_DataBuffer.RemoveRange(0, length + 4);

						//If more data available
						if (m_DataBuffer.Count > 4)
						{
							//Calculate new length
							bytes = m_DataBuffer.Take(4).ToArray();
							length = (int)BitConverter.ToInt32(bytes.ToArray(), 0);
						}
					}
				}
				catch (Exception ex)
				{
					//Empty the buffer
					m_DataBuffer.Clear();
					ExceptionAppeared(null, ex);
				}
			}
		}
	}
}
