﻿using ConvertTCPStream.Model;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Text;
using System.Windows;
using System.Xml.Serialization;
using VoIPCall.Model;

namespace VoIPCall.ViewModel
{
    /// <summary>
    /// This class contains properties that the main View can data bind to.
    /// <para>
    /// See http://www.mvvmlight.net
    /// </para>
    /// </summary>
    public class MainViewModel : ViewModelBase
    {
        #region Attributes
        private readonly IDataService _dataService;
        private BinaryFormat binaryFormat = new BinaryFormat();
        private Clients _Client;
        private Server _Server;
        private Configuration _Config = new Configuration();
        private int _SoundBufferCount = 8;
        private WinSound.Protocol _PrototolClient = new WinSound.Protocol(WinSound.ProtocolTypes.LH, Encoding.Default);
        private Dictionary<ServerThread, ThreadData> _DictionaryServerDatas = new Dictionary<ServerThread, ThreadData>();
        private WinSound.Recorder _Recorder_Client;
        private WinSound.Recorder _Recorder_Server;
        private WinSound.Player _PlayerClient;
        private uint _RecorderFactor = 4;
        private WinSound.JitterBuffer _JitterBufferClientRecording;
        private WinSound.JitterBuffer _JitterBufferClientPlaying;
        private WinSound.JitterBuffer _JitterBufferServerRecording;
        WinSound.WaveFileHeader _FileHeader = new WinSound.WaveFileHeader();
        private WinSound.EventTimer _TimerMixed = null;
        private uint _Milliseconds = 20;
        private Object LockerDictionary = new Object();
        public static Dictionary<Object, Queue<List<Byte>>> DictionaryMixed = new Dictionary<Object, Queue<List<byte>>>();
        private Encoding _Encoding = Encoding.GetEncoding(1252);
        private const int RecordingJitterBufferCount = 8;
        #endregion

        #region PropertyName

        #region SERVER
        /// <summary>
        /// The <see cref="ServerIP" /> property's name.
        /// </summary>
        public const string ServerIPPropertyName = "ServerIP";

        private string _ServerIP = string.Empty;

        /// <summary>
        /// Sets and gets the ServerIP property.
        /// Changes to that property's value raise the PropertyChanged event. 
        /// </summary>
        public string ServerIP
        {
            get
            {
                return _ServerIP;
            }

            set
            {
                if (_ServerIP == value)
                {
                    return;
                }

                _ServerIP = value;
                RaisePropertyChanged(ServerIPPropertyName);
            }
        }

        /// <summary>
        /// The <see cref="ServerPort" /> property's name.
        /// </summary>
        public const string ServerPortPropertyName = "ServerPort";

        private string _ServerPort = "0";

        /// <summary>
        /// Sets and gets the ServerPort property.
        /// Changes to that property's value raise the PropertyChanged event. 
        /// </summary>
        public string ServerPort
        {
            get
            {
                return _ServerPort;
            }

            set
            {
                if (_ServerPort == value)
                {
                    return;
                }

                _ServerPort = value;
                RaisePropertyChanged(ServerPortPropertyName);
            }
        }

        /// <summary>
        /// The <see cref="Servernotification" /> property's name.
        /// </summary>
        public const string ServernotificationPropertyName = "Servernotification";

        private string _Servernotification = string.Empty;

        /// <summary>
        /// Sets and gets the ErrorServer property.
        /// Changes to that property's value raise the PropertyChanged event. 
        /// </summary>
        public string Servernotification
        {
            get
            {
                return _Servernotification;
            }

            set
            {
                if (_Servernotification == value)
                {
                    return;
                }

                _Servernotification = value;
                RaisePropertyChanged(ServernotificationPropertyName);
            }
        }

        /// <summary>
        /// The <see cref="ColorbtnServer" /> property's name.
        /// </summary>
        public const string ColorbtnServerPropertyName = "ColorbtnServer";

        private string _ColorbtnServer = string.Empty;

        /// <summary>
        /// Sets and gets the ColorbtnServer property.
        /// Changes to that property's value raise the PropertyChanged event. 
        /// </summary>
        public string ColorbtnServer
        {
            get
            {
                return _ColorbtnServer;
            }

            set
            {
                if (_ColorbtnServer == value)
                {
                    return;
                }

                _ColorbtnServer = value;
                RaisePropertyChanged(ColorbtnServerPropertyName);
            }
        }

        /// <summary>
        /// The <see cref="TextbtnServer" /> property's name.
        /// </summary>
        public const string TextbtnServerPropertyName = "TextbtnServer";

        private string _TextbtnServer = string.Empty;

        /// <summary>
        /// Sets and gets the TextbtnServer property.
        /// Changes to that property's value raise the PropertyChanged event. 
        /// </summary>
        public string TextbtnServer
        {
            get
            {
                return _TextbtnServer;
            }

            set
            {
                if (_TextbtnServer == value)
                {
                    return;
                }

                _TextbtnServer = value;
                RaisePropertyChanged(TextbtnServerPropertyName);
            }
        }

        /// <summary>
        /// The <see cref="ColorServerNotification" /> property's name.
        /// </summary>
        public const string ColorServerNotificationPropertyName = "ColorServerNotification";

        private string _ColorServerNotification = string.Empty;

        /// <summary>
        /// Sets and gets the ColorNotification property.
        /// Changes to that property's value raise the PropertyChanged event. 
        /// </summary>
        public string ColorServerNotification
        {
            get
            {
                return _ColorServerNotification;
            }

            set
            {
                if (_ColorServerNotification == value)
                {
                    return;
                }

                _ColorServerNotification = value;
                RaisePropertyChanged(ColorServerNotificationPropertyName);
            }
        }

        /// <summary>
        /// The <see cref="ListClientIP" /> property's name.
        /// </summary>
        public const string ListClientIPPropertyName = "ListClientIP";

        private ObservableCollection<ClientConnected> _ListClientIP;

        /// <summary>
        /// Sets and gets the IPAddressClient property.
        /// Changes to that property's value raise the PropertyChanged event. 
        /// </summary>
        public ObservableCollection<ClientConnected> ListClientIP
        {
            get
            {
                return _ListClientIP;
            }

            set
            {
                if (_ListClientIP == value)
                {
                    return;
                }

                _ListClientIP = value;
                RaisePropertyChanged(ListClientIPPropertyName);
            }
        }

        /// <summary>
        /// The <see cref="CallingHistories" /> property's name.
        /// </summary>
        public const string CallingHistoriesPropertyName = "CallingHistories";

        private ObservableCollection<ClientConnected> _CallingHistories;

        /// <summary>
        /// Sets and gets the CallingHistories property.
        /// Changes to that property's value raise the PropertyChanged event. 
        /// </summary>
        public ObservableCollection<ClientConnected> CallingHistories
        {
            get
            {
                return _CallingHistories;
            }

            set
            {
                if (_CallingHistories == value)
                {
                    return;
                }

                _CallingHistories = value;
                RaisePropertyChanged(CallingHistoriesPropertyName);
            }
        }

        #endregion

        #region CLIENT
        /// <summary>
        /// The <see cref="ClientIP" /> property's name.
        /// </summary>
        public const string ClientIPPropertyName = "ClientIP";

        private string _ClientIP = string.Empty;

        /// <summary>
        /// Sets and gets the ClientIP property.
        /// Changes to that property's value raise the PropertyChanged event. 
        /// </summary>
        public string ClientIP
        {
            get
            {
                return _ClientIP;
            }

            set
            {
                if (_ClientIP == value)
                {
                    return;
                }

                _ClientIP = value;
                RaisePropertyChanged(ClientIPPropertyName);
            }
        }

        /// <summary>
        /// The <see cref="ClientPort" /> property's name.
        /// </summary>
        public const string ClientPortPropertyName = "ClientPort";

        private string _ClientPort = "0";

        /// <summary>
        /// Sets and gets the ClientPort property.
        /// Changes to that property's value raise the PropertyChanged event. 
        /// </summary>
        public string ClientPort
        {
            get
            {
                return _ClientPort;
            }

            set
            {
                if (_ClientPort == value)
                {
                    return;
                }

                _ClientPort = value;
                RaisePropertyChanged(ClientPortPropertyName);
            }
        }

        /// <summary>
        /// The <see cref="TextbtnClient" /> property's name.
        /// </summary>
        public const string TextbtnClientPropertyName = "TextbtnClient";

        private string _TextbtnClient = string.Empty;

        /// <summary>
        /// Sets and gets the TextbtnClient property.
        /// Changes to that property's value raise the PropertyChanged event. 
        /// </summary>
        public string TextbtnClient
        {
            get
            {
                return _TextbtnClient;
            }

            set
            {
                if (_TextbtnClient == value)
                {
                    return;
                }

                _TextbtnClient = value;
                RaisePropertyChanged(TextbtnClientPropertyName);
            }
        }

        /// <summary>
        /// The <see cref="ColorbtnClient" /> property's name.
        /// </summary>
        public const string ColorbtnClientPropertyName = "ColorbtnClient";

        private string _ColorbtnClient = string.Empty;

        /// <summary>
        /// Sets and gets the ColorbtnClient property.
        /// Changes to that property's value raise the PropertyChanged event. 
        /// </summary>
        public string ColorbtnClient
        {
            get
            {
                return _ColorbtnClient;
            }

            set
            {
                if (_ColorbtnClient == value)
                {
                    return;
                }

                _ColorbtnClient = value;
                RaisePropertyChanged(ColorbtnClientPropertyName);
            }
        }

        /// <summary>
        /// The <see cref="Clientnotification" /> property's name.
        /// </summary>
        public const string ClientnotificationPropertyName = "Clientnotification";

        private string _Clientnotification = string.Empty;

        /// <summary>
        /// Sets and gets the Clientnotification property.
        /// Changes to that property's value raise the PropertyChanged event. 
        /// </summary>
        public string Clientnotification
        {
            get
            {
                return _Clientnotification;
            }

            set
            {
                if (_Clientnotification == value)
                {
                    return;
                }

                _Clientnotification = value;
                RaisePropertyChanged(ClientnotificationPropertyName);
            }
        }

        /// <summary>
        /// The <see cref="ColorClientnotification" /> property's name.
        /// </summary>
        public const string ColorClientnotificationPropertyName = "ColorClientnotification";

        private string _ColorClientnotification = string.Empty;

        /// <summary>
        /// Sets and gets the ColorClientnotification property.
        /// Changes to that property's value raise the PropertyChanged event. 
        /// </summary>
        public string ColorClientnotification
        {
            get
            {
                return _ColorClientnotification;
            }

            set
            {
                if (_ColorClientnotification == value)
                {
                    return;
                }

                _ColorClientnotification = value;
                RaisePropertyChanged(ColorClientnotificationPropertyName);
            }
        }

        ///// <summary>
        ///// The <see cref="ListClientforClient" /> property's name.
        ///// </summary>
        //public const string ListClientforClientPropertyName = "ListClientforClient";

        //private ObservableCollection<ClientConnected> _ListClientforClient;

        ///// <summary>
        ///// Sets and gets the ListClientforClient property.
        ///// Changes to that property's value raise the PropertyChanged event. 
        ///// </summary>
        //public ObservableCollection<ClientConnected> ListClientforClient
        //{
        //    get
        //    {
        //        return _ListClientforClient;
        //    }

        //    set
        //    {
        //        if (_ListClientforClient == value)
        //        {
        //            return;
        //        }

        //        _ListClientforClient = value;
        //        RaisePropertyChanged(ListClientforClientPropertyName);
        //    }
        //}
        #endregion

        #region SoundDevice
        /// <summary>
        /// The <see cref="ListInputSoundDevice" /> property's name.
        /// </summary>
        public const string ListInputSoundDevicePropertyName = "ListInputSoundDevice";

        private ObservableCollection<string> _ListInputSoundDevice;

        /// <summary>
        /// Sets and gets the ClientIP property.
        /// Changes to that property's value raise the PropertyChanged event. 
        /// </summary>
        public ObservableCollection<string> ListInputSoundDevice
        {
            get
            {
                return _ListInputSoundDevice;
            }

            set
            {
                if (_ListInputSoundDevice == value)
                {
                    return;
                }

                _ListInputSoundDevice = value;
                RaisePropertyChanged(ListInputSoundDevicePropertyName);
            }
        }

        /// <summary>
        /// The <see cref="IndexInput" /> property's name.
        /// </summary>
        public const string IndexInputPropertyName = "IndexInput";

        private int _IndexInput = 0;

        /// <summary>
        /// Sets and gets the IndexInput property.
        /// Changes to that property's value raise the PropertyChanged event. 
        /// </summary>
        public int IndexInput
        {
            get
            {
                return _IndexInput;
            }

            set
            {
                if (_IndexInput == value)
                {
                    return;
                }

                _IndexInput = value;
                RaisePropertyChanged(IndexInputPropertyName);
            }
        }

        /// <summary>
        /// The <see cref="ListOutputSoundDevice" /> property's name.
        /// </summary>
        public const string ListOutputSoundDevicePropertyName = "ListOutputSoundDevice";

        private ObservableCollection<string> _ListOutputSoundDevice;

        /// <summary>
        /// Sets and gets the ClientIP property.
        /// Changes to that property's value raise the PropertyChanged event. 
        /// </summary>
        public ObservableCollection<string> ListOutputSoundDevice
        {
            get
            {
                return _ListOutputSoundDevice;
            }

            set
            {
                if (_ListOutputSoundDevice == value)
                {
                    return;
                }

                _ListOutputSoundDevice = value;
                RaisePropertyChanged(ListOutputSoundDevicePropertyName);
            }
        }

        /// <summary>
        /// The <see cref="IndexOutput" /> property's name.
        /// </summary>
        public const string IndexOutputPropertyName = "IndexOutput";

        private int _IndexOutput = 0;

        /// <summary>
        /// Sets and gets the IndexOutput property.
        /// Changes to that property's value raise the PropertyChanged event. 
        /// </summary>
        public int IndexOutput
        {
            get
            {
                return _IndexOutput;
            }

            set
            {
                if (_IndexOutput == value)
                {
                    return;
                }

                _IndexOutput = value;
                RaisePropertyChanged(IndexOutputPropertyName);
            }
        }

        #endregion
        #endregion

        #region RelayCommand
        private RelayCommand _ServerStart;

        /// <summary>
        /// Gets the MyCommand.
        /// </summary>
        public RelayCommand ServerStart
        {
            get
            {
                return _ServerStart
                    ?? (_ServerStart = new RelayCommand(
                    () =>
                    {
                        try
                        {
                            //Get data
                            FormToConfig();

                            if (IsServerRunning)
                            {
                                StopServer();
                                StopRecordingFromSounddevice_Server();
                                StopTimerMixed();
                            }
                            else
                            {
                                StartServer();
                                StartRecordingFromSounddevice_Server(); 
                                StartTimerMixed();
                            }
                        }
                        catch (Exception ex)
                        {
                            ShowError(Servernotification, ColorServerNotification, ex.Message);
                        }
                    }));
            }
        }

        private RelayCommand _StartClient;

        /// <summary>
        /// Gets the MyCommand.
        /// </summary>
        public RelayCommand StartClient
        {
            get
            {
                return _StartClient
                    ?? (_StartClient = new RelayCommand(
                    () =>
                    {
                        try
                        {
                            //Get data
                            FormToConfig();

                            if (IsClientConnected)
                            {
                                DisconnectClient();
                                StopRecordingFromSounddevice_Client();
                            }
                            else
                            {
                                ConnectClient();
                                string Name = Dns.GetHostName();
                                _Client.Send(binaryFormat.Seriaize(Name));
                            }

                            //Kurz warten
                            System.Threading.Thread.Sleep(100);
                        }
                        catch (Exception ex)
                        {
                            ShowError(Clientnotification, ColorClientnotification, ex.Message);
                        }
                    }));
            }
        }

        private RelayCommand<Window> _WindowClose;

        /// <summary>
        /// Gets the MyCommand.
        /// </summary>
        public RelayCommand<Window> WindowClose
        {
            get
            {
                return _WindowClose
                    ?? (_WindowClose = new RelayCommand<Window>(
                    p =>
                    {
                        StopRecordingFromSounddevice_Server();
                        StopRecordingFromSounddevice_Client();
                        DisconnectClient();
                        StopServer();
                        p.Close();
                    }));
            }
        }
        #endregion

        /// <summary>
        /// SetUpializes a new instance of the MainViewModel class.
        /// </summary>
        public MainViewModel(IDataService dataService)
        {
            _dataService = dataService;
            _dataService.GetServer(
                (item, error) =>
                {
                    if (error != null)
                    {
                        // Report error here
                        return;
                    }

                    ServerIP = item.IP;
                    ServerPort = item.PORT;
                });
            _dataService.GetButtonServer(
                (item, error) =>
                {
                    if (error != null)
                    {
                        // Report error here
                        return;
                    }

                    TextbtnServer = item.Text;
                    ColorbtnServer = item.Color;
                });
            _dataService.GetButtonClient(
                (item, error) =>
                {
                    if (error != null)
                    {
                        // Report error here
                        return;
                    }

                    TextbtnClient = item.Text;
                    ColorbtnClient = item.Color;
                });
            _dataService.GetNotification(
                (item, error) =>
                {
                    if (error != null)
                    {
                        // Report error here
                        return;
                    }

                    Servernotification = item.Text;
                    Clientnotification = item.Text;
                    ColorServerNotification = item.Color;
                    ColorClientnotification = item.Color;
                });


            ListClientIP = new ObservableCollection<ClientConnected>();
            ListClientIP.Clear();
            CallingHistories = new ObservableCollection<ClientConnected>();
            CallingHistories.Clear();
            //ListClientforClient = new ObservableCollection<ClientConnected>();
            //ListClientforClient.Clear();
            ListInputSoundDevice = new ObservableCollection<string>();
            ListInputSoundDevice.Clear();
            ListOutputSoundDevice = new ObservableCollection<string>();
            ListOutputSoundDevice.Clear();
            SetUp();
        }


        #region IsBool
        /// <summary>
        ///IsServerRunning 
        /// </summary>
        private bool IsServerRunning
        {
            get
            {
                if (_Server != null)
                {
                    return _Server.State == Server.ListenerState.Started;
                }
                return false;
            }
        }

        /// <summary>
		/// IsClientConnected
		/// </summary>
		private bool IsClientConnected
        {
            get
            {
                if (_Client != null)
                {
                    return _Client.Connected;
                }
                return false;
            }
        }

        /// <summary>
		/// IsPlayingToSoundDeviceWanted
		/// </summary>
		private bool IsPlayingToSoundDeviceWanted
        {
            get
            {
                if (IndexOutput >= 0)
                {
                    return true;
                }
                return false;
            }
        }

        /// <summary>
		/// UseJitterBuffer
		/// </summary>
		private bool UseJitterBufferServer
        {
            get
            {
                return _Config.JitterBufferCountServer >= 2;
            }
        }

        /// <summary>
		/// UseJitterBufferServerRecording
		/// </summary>
		private bool UseJitterBufferServerRecording
        {
            get
            {
                return _Config.UseJitterBufferServerRecording;
            }
        }

        /// <summary>
		/// IsRecorderStarted
		/// </summary>
		private bool IsRecorderFromSounddeviceStarted_Client
        {
            get
            {
                if (_Recorder_Client != null)
                {
                    return _Recorder_Client.Started;
                }
                return false;
            }
        }

        /// <summary>
		/// IsRecorderFromSounddeviceStarted_Server
		/// </summary>
		private bool IsRecorderFromSounddeviceStarted_Server
        {
            get
            {
                if (_Recorder_Server != null)
                {
                    return _Recorder_Server.Started;
                }
                return false;
            }
        }


        /// <summary>
        /// UseJitterBuffer
        /// </summary>
        private bool UseJitterBufferClientRecording
        {
            get
            {
                return _Config.UseJitterBufferClientRecording;
            }
        }

        #endregion

        #region Method
        private void ShowStart()
        {
            TextbtnServer = "STOP";
            ColorbtnServer = "Red";
            Servernotification = "Server is starting";
            ColorServerNotification = "#000000";
        }

        private void ShowStop()
        {
            TextbtnServer = "START";
            ColorbtnServer = "#FF2196F3";
            Servernotification = "Server has Stoped";
            ColorServerNotification = "#000000";
        }

        private void ShowConnected()
        {
            TextbtnClient = "_DISCONNECT";
            ColorbtnClient = "Red";
            Clientnotification = "Client Connected to " + ClientIP;
            ColorClientnotification = "#000000";
        }

        private void ShowDisconnect()
        {
            TextbtnClient = "_CONNECT";
            ColorbtnClient = "#FF2196F3";
        }

        private void ShowError(string lb, string Color, string text)
        {
            try
            {
                lb = text;
                Color = "Red";
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        private void ShowMessage(string lb, string color, string text)
        {
            try
            {

                lb = text;
                color = "Black";
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        private bool FormToConfig()
        {
            try
            {
                _Config.IpAddressClient = ClientIP;
                _Config.IPAddressServer = ServerIP;
                _Config.PortClient = Convert.ToInt32(ClientPort);
                _Config.PortServer = Convert.ToInt32(ServerPort);
                _Config.SoundInputDeviceNameClient = IndexInput >= 0 ? ListInputSoundDevice.ElementAt(IndexInput).ToString() : "";
                _Config.SoundOutputDeviceNameClient = IndexOutput >= 0 ? ListOutputSoundDevice.ElementAt(IndexOutput).ToString() : "";
                _Config.SoundInputDeviceNameServer = IndexInput >= 0 ? ListInputSoundDevice.ElementAt(IndexInput).ToString() : "";
                _Config.SoundOutputDeviceNameServer = IndexOutput >= 0 ? ListOutputSoundDevice.ElementAt(IndexOutput).ToString() : "";
                _Config.JitterBufferCountServer = 20;
                _Config.JitterBufferCountClient = 20;
                _Config.SamplesPerSecondServer = 8000;
                _Config.BitsPerSampleServer = 16;
                _Config.BitsPerSampleClient = 16;
                _Config.ChannelsServer = 1;
                _Config.ChannelsClient = 1;
                _Config.UseJitterBufferClientRecording = true;
                _Config.UseJitterBufferServerRecording = true;
                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Thông Báo");
                return false;
            }
        }

        /// <summary>
        /// ToRTPPacket
        /// </summary>
        /// <param name="linearData"></param>
        /// <param name="bitsPerSample"></param>
        /// <param name="channels"></param>
        /// <returns></returns>
        private WinSound.RTPPacket ToRTPPacket(Byte[] linearData, int bitsPerSample, int channels)
        {
            //Convert data to MuLaw
            Byte[] mulaws = WinSound.Utils.LinearToMulaw(linearData, bitsPerSample, channels);

            //New Create RTP Packet
            WinSound.RTPPacket rtp = new WinSound.RTPPacket();

            //take values
            rtp.Data = mulaws;
            rtp.HeaderLength = WinSound.RTPPacket.MinHeaderLength;
            //Finished
            return rtp;
        }

        /// <summary>
		/// ToRTPData
		/// </summary>
		/// <param name="linearData"></param>
		/// <param name="bitsPerSample"></param>
		/// <param name="channels"></param>
		/// <returns></returns>
		private Byte[] ToRTPData(Byte[] data, int bitsPerSample, int channels)
        {
            //New Create RTP Packet
            WinSound.RTPPacket rtp = ToRTPPacket(data, bitsPerSample, channels);
            //Create RTPHeader in Bytes
            Byte[] rtpBytes = rtp.ToBytes();
            //Finished
            return rtpBytes;
        }

        #region Set up Config
        /// <summary>
        /// SetUp
        /// </summary>
        private void SetUp()
        {
            try
            {
                SetUpComboboxes();
                SetUpJitterBufferClientRecording();
                SetUpJitterBufferClientPlaying();
                SetUpJitterBufferServerRecording();
                SetUpProtocolClient();
            }
            catch (Exception ex)
            {
                ShowError(Clientnotification, ColorClientnotification, ex.Message);
            }
        }

        #region Show Sound Device
        /// <summary>
        /// SetUpComboboxes
        /// </summary>
        private void SetUpComboboxes()
        {
            SetUpComboboxesClient();
            SetUpComboboxesServer();
        }

        /// <summary>
		/// SetUpComboboxesClient
		/// </summary>
		private void SetUpComboboxesClient()
        {
            ListOutputSoundDevice.Clear();
            ListInputSoundDevice.Clear();
            List<String> playbackNames = WinSound.WinSound.GetPlaybackNames();
            List<String> recordingNames = WinSound.WinSound.GetRecordingNames();

            //Output
            foreach (String name in playbackNames.Where(x => x != null))
            {
                ListOutputSoundDevice.Add(name);
            }
            //Input
            foreach (String name in recordingNames.Where(x => x != null))
            {
                ListInputSoundDevice.Add(name);
            }

            //Output
            if (ListOutputSoundDevice.Count > 0)
            {
                IndexOutput = 0;
            }
            //Input
            if (ListInputSoundDevice.Count > 0)
            {
                IndexInput = 0;
            }
        }

        /// <summary>
		/// SetUpComboboxesServer
		/// </summary>
		private void SetUpComboboxesServer()
        {
            ListOutputSoundDevice.Clear();
            ListInputSoundDevice.Clear();
            List<String> playbackNames = WinSound.WinSound.GetPlaybackNames();
            List<String> recordingNames = WinSound.WinSound.GetRecordingNames();

            //Output
            foreach (String name in playbackNames.Where(x => x != null))
            {
                ListOutputSoundDevice.Add(name);
            }
            //Input
            foreach (String name in recordingNames.Where(x => x != null))
            {
                ListInputSoundDevice.Add(name);
            }

            //Output
            if (ListOutputSoundDevice.Count > 0)
            {
                IndexOutput = 0;
            }
            //Input
            if (ListInputSoundDevice.Count > 0)
            {
                IndexInput = 0;
            }
        }

        #endregion

        #region Set Jitter Recording Buffer For Client
        /// <summary>
        /// SetUpJitterBufferClientRecording
        /// </summary>
        private void SetUpJitterBufferClientRecording()
        {
            //If available
            if (_JitterBufferClientRecording != null)
            {
                _JitterBufferClientRecording.DataAvailable -= new WinSound.JitterBuffer.DelegateDataAvailable(OnJitterBufferClientDataAvailableRecording);
            }

            //Create new
            _JitterBufferClientRecording = new WinSound.JitterBuffer(null, RecordingJitterBufferCount, 20);
            _JitterBufferClientRecording.DataAvailable += new WinSound.JitterBuffer.DelegateDataAvailable(OnJitterBufferClientDataAvailableRecording);
        }

        /// <summary>
		/// OnJitterBufferClientDataAvailable
		/// </summary>
		/// <param name="rtp"></param>
		private void OnJitterBufferClientDataAvailableRecording(Object sender, WinSound.RTPPacket rtp)
        {
            try
            {
                //Check
                if (rtp != null && _Client != null && rtp.Data != null && rtp.Data.Length > 0)
                {
                    if (IsClientConnected)
                    {
                        //Convert RTP Packet to Bytes
                        Byte[] rtpBytes = rtp.ToBytes();
                        //AbSend
                        _Client.Send(_PrototolClient.ToBytes(rtpBytes));

                    }
                }
            }
            catch (Exception ex)
            {
                System.Diagnostics.StackFrame sf = new System.Diagnostics.StackFrame(true);
                ShowError(Clientnotification, ColorClientnotification, String.Format("Exception: {0} StackTrace: {1}. FileName: {2} Method: {3} Line: {4}", ex.Message, ex.StackTrace, sf.GetFileName(), sf.GetMethod(), sf.GetFileLineNumber()));
            }
        }
        #endregion

        #region Set Jitter Playing Buffer For Client
        /// <summary>
		/// SetUpJitterBufferClientPlaying
		/// </summary>
		private void SetUpJitterBufferClientPlaying()
        {
            //If available
            if (_JitterBufferClientPlaying != null)
            {
                _JitterBufferClientPlaying.DataAvailable -= new WinSound.JitterBuffer.DelegateDataAvailable(OnJitterBufferClientDataAvailablePlaying);
            }

            //Create new
            _JitterBufferClientPlaying = new WinSound.JitterBuffer(null, _Config.JitterBufferCountClient, 20);
            _JitterBufferClientPlaying.DataAvailable += new WinSound.JitterBuffer.DelegateDataAvailable(OnJitterBufferClientDataAvailablePlaying);
        }


        /// <summary>
        /// OnJitterBufferClientDataAvailablePlaying
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="rtp"></param>
        private void OnJitterBufferClientDataAvailablePlaying(Object sender, WinSound.RTPPacket rtp)
        {
            try
            {
                if (_PlayerClient != null)
                {
                    if (_PlayerClient.Opened)
                    {
                        //Convert to Linear
                        Byte[] linearBytes = WinSound.Utils.MuLawToLinear(rtp.Data, _Config.BitsPerSampleClient, _Config.ChannelsClient);
                        //Play
                        _PlayerClient.PlayData(linearBytes, false);


                    }
                }
            }
            catch (Exception ex)
            {
                System.Diagnostics.StackFrame sf = new System.Diagnostics.StackFrame(true);
                ShowError(Clientnotification, ColorClientnotification, String.Format("Exception: {0} StackTrace: {1}. FileName: {2} Method: {3} Line: {4}", ex.Message, ex.StackTrace, sf.GetFileName(), sf.GetMethod(), sf.GetFileLineNumber()));
            }
        }

        #endregion

        #region Set Jitter Recording Buffer For Server
        /// <summary>
		/// SetUpJitterBuffer
		/// </summary>
		private void SetUpJitterBufferServerRecording()
        {
            //If available
            if (_JitterBufferServerRecording != null)
            {
                _JitterBufferServerRecording.DataAvailable -= new WinSound.JitterBuffer.DelegateDataAvailable(OnJitterBufferServerDataAvailable);
            }

            //Create new
            _JitterBufferServerRecording = new WinSound.JitterBuffer(null, RecordingJitterBufferCount, 20);
            _JitterBufferServerRecording.DataAvailable += new WinSound.JitterBuffer.DelegateDataAvailable(OnJitterBufferServerDataAvailable);
        }

        /// <summary>
		/// OnJitterBufferServerDataAvailable
		/// </summary>
		/// <param name="rtp"></param>
		private void OnJitterBufferServerDataAvailable(Object sender, WinSound.RTPPacket rtp)
        {
            try
            {
                if (IsServerRunning)
                {
                    //Convert RTP Packet to Bytes
                    Byte[] rtpBytes = rtp.ToBytes();

                    //For all clients
                    List<ServerThread> list = new List<ServerThread>(_Server.Clients);
                    foreach (ServerThread client in list)
                    {
                        //If not mute
                        if (client.IsMute == false)
                        {
                            try
                            {
                                //AbSend
                                client.Send(_PrototolClient.ToBytes(rtpBytes));
                            }
                            catch (Exception)
                            {
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                System.Diagnostics.StackFrame sf = new System.Diagnostics.StackFrame(true);
                ShowError(Servernotification, ColorServerNotification, String.Format("Exception: {0} StackTrace: {1}. FileName: {2} Method: {3} Line: {4}", ex.Message, ex.StackTrace, sf.GetFileName(), sf.GetMethod(), sf.GetFileLineNumber()));
            }
        }

        #endregion

        #region Set Protocol
        /// <summary>
        /// SetUpProtocolClient
        /// </summary>
        private void SetUpProtocolClient()
        {
            if (_PrototolClient != null)
            {
                _PrototolClient.DataComplete += new WinSound.Protocol.DelegateDataComplete(OnProtocolClient_DataComplete);
            }
        }

        /// <summary>
		/// OnProtocolClient_DataComplete
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="data"></param>
		private void OnProtocolClient_DataComplete(Object sender, Byte[] data)
        {
            try
            {
                //When the player has started
                if (_PlayerClient != null)
                {
                    if (_PlayerClient.Opened)
                    {
                        //Read out RTP header
                        WinSound.RTPPacket rtp = new WinSound.RTPPacket(data);

                        //If header is correct
                        if (rtp.Data != null)
                        {
                            //Add in JitterBuffer
                            if (_JitterBufferClientPlaying != null)
                            {
                                _JitterBufferClientPlaying.AddData(rtp);
                            }
                        }
                    }
                }
                else
                {
                    //Obtained configuration data
                    OnClientConfigReceived(sender, data);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
        #endregion
        #endregion

        #region Method Of Server
        /// <summary>
		/// StartServer
		/// </summary>
		private void StartServer()
        {
            try
            {
                if (IsServerRunning == false)
                {
                    if (_Config.IPAddressServer.Length > 0 && _Config.PortServer > 0)
                    {
                        _Server = new Server();
                        _Server.ClientConnected += new Server.DelegateClientConnected(OnServerClientConnected);
                        _Server.ClientDisconnected += new Server.DelegateClientDisconnected(OnServerClientDisconnected);
                        _Server.DataReceived += new Server.DelegateDataReceived(OnServerDataReceived);
                        _Server.Start(_Config.IPAddressServer, _Config.PortServer);

                        //Depending on server status
                        if (_Server.State == Server.ListenerState.Started)
                        {
                            ShowStart();
                        }
                        else
                        {
                            ShowStop();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ShowError(Clientnotification, ColorClientnotification, ex.Message);
            }
        }

        /// <summary>
        /// StopServer
        /// </summary>
        private void StopServer()
        {
            try
            {
                if (IsServerRunning == true)
                {
                    DeleteAllServerThreadDatas();
                    ListClientIP.Clear();
                    _Server.Stop();
                    _Server.ClientConnected -= new Server.DelegateClientConnected(OnServerClientConnected);
                    _Server.ClientDisconnected -= new Server.DelegateClientDisconnected(OnServerClientDisconnected);
                    _Server.DataReceived -= new Server.DelegateDataReceived(OnServerDataReceived);
                }
                
                if (_Server != null)
                {
                    if (_Server.State == Server.ListenerState.Started)
                    {
                        ShowStart();
                    }
                    else
                    {
                        ShowStop();
                    }
                }
                
                _Server = null;
            }
            catch (Exception ex)
            {
                ShowError(Servernotification, ColorServerNotification, ex.Message);
            }
        }

        /// <summary>
		/// StartRecordingFromSounddevice_Server
		/// </summary>
		private void StartRecordingFromSounddevice_Server()
        {
            try
            {
                if (IsRecorderFromSounddeviceStarted_Server == false)
                {
                    //Buffer calculate size
                    int bufferSize = 0;
                    if (UseJitterBufferServerRecording)
                    {
                        bufferSize = WinSound.Utils.GetBytesPerInterval((uint)_Config.SamplesPerSecondServer, _Config.BitsPerSampleServer, _Config.ChannelsServer) * (int)_RecorderFactor;
                    }
                    else
                    {
                        bufferSize = WinSound.Utils.GetBytesPerInterval((uint)_Config.SamplesPerSecondServer, _Config.BitsPerSampleServer, _Config.ChannelsServer);
                    }

                    //If buffer is correct
                    if (bufferSize > 0)
                    {
                        //Create a recorder
                        _Recorder_Server = new WinSound.Recorder();

                        //Add events
                        _Recorder_Server.DataRecorded += new WinSound.Recorder.DelegateDataRecorded(OnDataReceivedFromSoundcard_Server);

                        //Start recorder
                        if (_Recorder_Server.Start(_Config.SoundInputDeviceNameServer, _Config.SamplesPerSecondServer, _Config.BitsPerSampleServer, _Config.ChannelsServer, _SoundBufferCount, bufferSize))
                        {
                            //Add to mixer
                            MainViewModel.DictionaryMixed[this] = new Queue<List<byte>>();

                            //Start JitterBuffer
                            _JitterBufferServerRecording.Start();
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                ShowError(Clientnotification, ColorClientnotification, ex.Message);
            }
        }

        /// <summary>
		/// StopRecordingFromSounddevice_Server
		/// </summary>
		private void StopRecordingFromSounddevice_Server()
        {
            try
            {
                if (IsRecorderFromSounddeviceStarted_Server)
                {
                    //To stop
                    _Recorder_Server.Stop();

                    //Remove events
                    _Recorder_Server.DataRecorded -= new WinSound.Recorder.DelegateDataRecorded(OnDataReceivedFromSoundcard_Server);
                    _Recorder_Server = null;

                    //JitterBuffer break up
                    _JitterBufferServerRecording.Stop();
                }
            }
            catch (Exception ex)
            {
                ShowError(Clientnotification, ColorClientnotification, ex.Message);
            }
        }

        /// <summary>
        /// StartTimerMixed
        /// </summary>
        private void StartTimerMixed()
        {
            if (_TimerMixed == null)
            {
                _TimerMixed = new WinSound.EventTimer();
                _TimerMixed.TimerTick += new WinSound.EventTimer.DelegateTimerTick(OnTimerSendMixedDataToAllClients);
                _TimerMixed.Start(20, 0);
            }
        }

        /// <summary>
        /// StopTimerMixed
        /// </summary>
        private void StopTimerMixed()
        {
            if (_TimerMixed != null)
            {
                _TimerMixed.Stop();
                _TimerMixed.TimerTick -= new WinSound.EventTimer.DelegateTimerTick(OnTimerSendMixedDataToAllClients);
                _TimerMixed = null;
            }
        }

        /// <summary>
        /// OnServerClientConnected
        /// </summary>
        /// <param name="st"></param>
        private void OnServerClientConnected(ServerThread st)
        {
            try
            {
                //Create ServerThread data
                ThreadData data = new ThreadData();
                data.Init(st, _Config.SoundOutputDeviceNameServer, _Config.SamplesPerSecondServer, _Config.BitsPerSampleServer, _Config.ChannelsServer, _SoundBufferCount, _Config.JitterBufferCountServer, _Milliseconds);
                
                //Add
                _DictionaryServerDatas[st] = data;

                //Add to List
                List_ServerClient(st);

                //Configuration Send
                SendConfigurationToClient(data);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        private void List_ServerClient(ServerThread st)
        {
            try
            {
                string ipaddress = st.Client.Client.RemoteEndPoint.ToString();
                string Data = (string)binaryFormat.Deserilize(_Server.recevie(st.Client));
                DateTime aDateTime = DateTime.Now;
                ClientConnected cc = new ClientConnected() { NameIPConnected = Data, IDAddress = ipaddress, StatusClient = "Connected" };
                ClientConnected cc1 = new ClientConnected() { NameIPConnected = Data, IDAddress = ipaddress, StatusClient = "" + aDateTime };
                App.Current.Dispatcher.Invoke((Action)delegate
                {
                    ListClientIP.Add(cc);
                    CallingHistories.Add(cc1);
                });
            }
            catch (Exception ex)
            {
                ColorServerNotification = "Red";
                Servernotification = ex.Message;
                return;
            }
        }

        /// <summary>
        /// SendConfigurationToClient
        /// </summary>
        /// <param name="st"></param>
        private void SendConfigurationToClient(ThreadData data)
        {
            if (MessageBox.Show("Bạn có chấp nhận trả lời cuộc gọi của: " + ListClientIP[ListClientIP.Count - 1].NameIPConnected, "Cuộc gọi từ IP " + ListClientIP[ListClientIP.Count - 1].IDAddress, MessageBoxButton.YesNo) == MessageBoxResult.Yes)
            {
                // You are Accept
                Byte[] bytesConfig = _Encoding.GetBytes(String.Format("Accept:{0}", _Config.SamplesPerSecondServer));
                data.ServerThread.Send(_PrototolClient.ToBytes(bytesConfig));
            }
            else
            {
                // You aren't Accept
                Byte[] bytesConfig = _Encoding.GetBytes(String.Format("NotAccept:{0}", _Config.SamplesPerSecondServer));
                data.ServerThread.Send(_PrototolClient.ToBytes(bytesConfig));
            }
        }

        /// <summary>
        /// OnServerClientDisconnected
        /// </summary>
        /// <param name="st"></param>
        /// <param name="info"></param>
        private void OnServerClientDisconnected(ServerThread st, string info)
        {
            try
            {
                //If available
                if (_DictionaryServerDatas.ContainsKey(st))
                {
                    //All release data
                    ThreadData data = _DictionaryServerDatas[st];
                    data.Dispose();
                    lock (LockerDictionary)
                    {
                        //Remove
                        _DictionaryServerDatas.Remove(st);
                    }
                    //From FlowLayoutPanels Remove
                    RemoveList_ServerClient(st);
                }

                //From Mix Data Remove
                MainViewModel.DictionaryMixed.Remove(st);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        private void RemoveList_ServerClient(ServerThread st)
        {
            try
            {
                string ipaddress = st.Client.Client.RemoteEndPoint.ToString();
                App.Current.Dispatcher.Invoke((Action)delegate
                {
                    for (int i = 0; i < ListClientIP.Count; i++)
                    {
                        if (ListClientIP[i].IDAddress == ipaddress)
                        {
                            ListClientIP.RemoveAt(i);
                        }
                    }
                });
            }
            catch (Exception ex)
            {
                ColorServerNotification = "Red";
                Servernotification = ex.Message;
                return;
            }
        }

        /// <summary>
        /// OnServerDataReceived
        /// </summary>
        /// <param name="st"></param>
        /// <param name="data"></param>
        private void OnServerDataReceived(ServerThread st, Byte[] data)
        {
            //If available
            if (_DictionaryServerDatas.ContainsKey(st))
            {
                // If protocol
                ThreadData stData = _DictionaryServerDatas[st];
                if (stData.Protocol != null)
                {
                    stData.Protocol.Receive_LH(st, data);
                }
            }
        }

        /// <summary>
		/// OnDataReceivedFromSoundcard_Server
		/// </summary>
		/// <param name="data"></param>
		private void OnDataReceivedFromSoundcard_Server(Byte[] data)
        {
            try
            {
                lock (this)
                {
                    if (IsServerRunning)
                    {
                        //Disassemble sound data into smaller parts
                        int bytesPerInterval = WinSound.Utils.GetBytesPerInterval((uint)_Config.SamplesPerSecondServer, _Config.BitsPerSampleServer, _Config.ChannelsServer);
                        int count = data.Length / bytesPerInterval;
                        int currentPos = 0;
                        for (int i = 0; i < count; i++)
                        {
                            //Convert section to RTP packet
                            Byte[] partBytes = new Byte[bytesPerInterval];
                            Array.Copy(data, currentPos, partBytes, 0, bytesPerInterval);
                            currentPos += bytesPerInterval;

                            //If buffer is not too big
                            Queue<List<Byte>> q = MainViewModel.DictionaryMixed[this];
                            if (q.Count < 10)
                            {
                                //Put data in blender
                                q.Enqueue(new List<Byte>(partBytes));
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine(ex.Message);
            }
        }

        /// <summary>
        /// OnTimerSendMixedDataToAllClients
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="e"></param>
        private void OnTimerSendMixedDataToAllClients()
        {
            try
            {
                //List with all language data (own + clients)
                Dictionary<Object, List<Byte>> dic = new Dictionary<object, List<byte>>();
                List<List<byte>> listlist = new List<List<byte>>();
                Dictionary<Object, Queue<List<Byte>>> copy = new Dictionary<object, Queue<List<byte>>>(DictionaryMixed);
                {
                    Queue<List<byte>> q = null;
                    foreach (Object obj in copy.Keys)
                    {

                        q = copy[obj];

                        //If data exists
                        if (q.Count > 0)
                        {
                            dic[obj] = q.Dequeue();
                            listlist.Add(dic[obj]);
                        }
                    }
                }

                if (listlist.Count > 0)
                {
                    //Mixed voice data
                    Byte[] mixedBytes = WinSound.Mixer.MixBytes(listlist, _Config.BitsPerSampleServer).ToArray();
                    List<Byte> listMixed = new List<Byte>(mixedBytes);

                    //For all clients
                    foreach (ServerThread client in _Server.Clients)
                    {
                        //If not dumb
                        if (client.IsMute == false)
                        {
                            //Mixed language for client
                            Byte[] mixedBytesClient = mixedBytes;

                            if (dic.ContainsKey(client))
                            {
                                //Determine the language of the client
                                List<Byte> listClient = dic[client];

                                //Subtract client language from Mix
                                mixedBytesClient = WinSound.Mixer.SubsctractBytes_16Bit(listMixed, listClient).ToArray();
                            }

                            //Create RTP Packet
                            WinSound.RTPPacket rtp = ToRTPPacket(mixedBytesClient, _Config.BitsPerSampleServer, _Config.ChannelsServer);
                            Byte[] rtpBytes = rtp.ToBytes();

                            //AbSend
                            client.Send(_PrototolClient.ToBytes(rtpBytes));
                        }
                    }
                }
            }

            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
        #endregion


        #region Method Of Client
        /// <summary>
        /// ConnectClient
        /// </summary>
        private void ConnectClient()
        {
            try
            {
                if (IsClientConnected == false)
                {
                    //If input exists
                    if (_Config.IpAddressClient.Length > 0 && _Config.PortClient > 0)
                    {
                        _Client = new Clients(_Config.IpAddressClient, _Config.PortClient);
                        _Client.ClientConnected += new Clients.DelegateConnection(OnClientConnected);
                        _Client.ClientDisconnected += new Clients.DelegateConnection(OnClientDisconnected);
                        _Client.ExceptionAppeared += new Clients.DelegateException(OnClientExceptionAppeared);
                        _Client.DataReceived += new Clients.DelegateDataReceived(OnClientDataReceived);
                        _Client.Connect();
                    }
                }
            }
            catch (Exception ex)
            {
                _Client = null;
                ShowError(Clientnotification, ColorClientnotification, ex.Message);
            }
        }

        /// <summary>
		/// DisconnectClient
		/// </summary>
		private void DisconnectClient()
        {
            try
            {
                StopRecordingFromSounddevice_Client();

                if (_Client != null)
                {
                    //Client break up
                    _Client.Disconnect();
                    _Client.ClientConnected -= new Clients.DelegateConnection(OnClientConnected);
                    _Client.ClientDisconnected -= new Clients.DelegateConnection(OnClientDisconnected);
                    _Client.ExceptionAppeared -= new Clients.DelegateException(OnClientExceptionAppeared);
                    _Client.DataReceived -= new Clients.DelegateDataReceived(OnClientDataReceived);
                    _Client = null;
                }
            }
            catch (Exception ex)
            {
                ShowError(Clientnotification, ColorClientnotification, ex.Message);
            }
        }

        /// <summary>
		/// StopRecordingFromSounddevice_Client
		/// </summary>
		private void StopRecordingFromSounddevice_Client()
        {
            try
            {
                if (IsRecorderFromSounddeviceStarted_Client)
                {
                    //To stop
                    _Recorder_Client.Stop();

                    //Remove events
                    _Recorder_Client.DataRecorded -= new WinSound.Recorder.DelegateDataRecorded(OnDataReceivedFromSoundcard_Client);
                    _Recorder_Client = null;

                    //If jitterbuffer
                    if (UseJitterBufferClientRecording)
                    {
                        _JitterBufferClientRecording.Stop();
                    }
                }
            }
            catch (Exception ex)
            {
                ShowError(Clientnotification, ColorClientnotification, ex.Message);
            }
        }

        /// <summary>
		/// OnClientConnected
		/// </summary>
		/// <param name="client"></param>
		/// <param name="info"></param>
		private void OnClientConnected(Clients client, string info)
        {
            ShowMessage(Clientnotification, ColorClientnotification, String.Format("Client connected {0}", ""));
            ShowConnected();
        }

        /// <summary>
        /// OnClientDisconnected
        /// </summary>
        /// <param name="client"></param>
        /// <param name="info"></param>
        private void OnClientDisconnected(Clients client, string info)
        {
            //Play break up
            StopPlayingToSounddevice_Client();
            //Stream sounddevice break up
            StopRecordingFromSounddevice_Client();

            if (_Client != null)
            {
                _Client.ClientConnected -= new Clients.DelegateConnection(OnClientConnected);
                _Client.ClientDisconnected -= new Clients.DelegateConnection(OnClientDisconnected);
                _Client.ExceptionAppeared -= new Clients.DelegateException(OnClientExceptionAppeared);
                _Client.DataReceived -= new Clients.DelegateDataReceived(OnClientDataReceived);
                ShowMessage(Clientnotification, ColorClientnotification, String.Format("Client disconnected {0}", ""));
            }

            ShowDisconnect();
        }

        /// <summary>
		/// OnClientExceptionAppeared
		/// </summary>
		/// <param name="client"></param>
		/// <param name="ex"></param>
		private void OnClientExceptionAppeared(Clients client, Exception ex)
        {
            DisconnectClient();
            ShowError(Clientnotification, ColorClientnotification, ex.Message);
        }

        /// <summary>
        /// OnClientDataReceived
        /// </summary>
        /// <param name="client"></param>
        /// <param name="bytes"></param>
        private void OnClientDataReceived(Clients client, Byte[] bytes)
        {
            try
            {
                if (_PrototolClient != null)
                {
                    _PrototolClient.Receive_LH(client, bytes);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        /// <summary>
		/// StartPlayingToSounddevice_Client
		/// </summary>
		private void StartPlayingToSounddevice_Client()
        {
            //If desired
            if (IsPlayingToSoundDeviceWanted)
            {
                //Start JitterBuffer
                if (_JitterBufferClientPlaying != null)
                {
                    SetUpJitterBufferClientPlaying();
                    _JitterBufferClientPlaying.Start();
                }

                if (_PlayerClient == null)
                {
                    _PlayerClient = new WinSound.Player();
                    _PlayerClient.Open(_Config.SoundOutputDeviceNameClient, _Config.SamplesPerSecondClient, _Config.BitsPerSampleClient, _Config.ChannelsClient, (int)_Config.JitterBufferCountClient);
                }
            }
        }

        /// <summary>
        /// OnClientConfigReceived
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="data"></param>
        private void OnClientConfigReceived(Object sender, Byte[] data)
        {
            try
            {
                String msg = _Encoding.GetString(data);
                if (msg.Length > 0)
                {
                    //parsing
                    String[] values = msg.Split(':');
                    String cmd = values[0];

                    //Depending on the command
                    switch (cmd.ToUpper())
                    {
                        case "ACCEPT":
                            int samplePerSecond = Convert.ToInt32(values[1]);
                            _Config.SamplesPerSecondClient = samplePerSecond;
                            StartPlayingToSounddevice_Client();
                            StartRecordingFromSounddevice_Client();
                            break;
                        case "NOTACCEPT":
                            DisconnectClient();
                            break;
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        /// <summary>
		/// DeleteAllServerThreadDatas
		/// </summary>
		private void DeleteAllServerThreadDatas()
        {
            lock (LockerDictionary)
            {
                try
                {
                    foreach (ThreadData info in _DictionaryServerDatas.Values)
                    {
                        info.Dispose();
                    }
                    _DictionaryServerDatas.Clear();
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
            }
        }

        /// <summary>
        /// StartRecordingFromSounddevice_Client
        /// </summary>
        private void StartRecordingFromSounddevice_Client()
        {
            try
            {
                if (IsRecorderFromSounddeviceStarted_Client == false)
                {
                    //Buffer calculate size
                    int bufferSize = 0;
                    if (UseJitterBufferClientRecording)
                    {
                        bufferSize = WinSound.Utils.GetBytesPerInterval((uint)_Config.SamplesPerSecondClient, _Config.BitsPerSampleClient, _Config.ChannelsClient) * (int)_RecorderFactor;
                    }
                    else
                    {
                        bufferSize = WinSound.Utils.GetBytesPerInterval((uint)_Config.SamplesPerSecondClient, _Config.BitsPerSampleClient, _Config.ChannelsClient);
                    }

                    //If buffer is correct
                    if (bufferSize > 0)
                    {
                        //Create a recorder
                        _Recorder_Client = new WinSound.Recorder();

                        //Add events
                        _Recorder_Client.DataRecorded += new WinSound.Recorder.DelegateDataRecorded(OnDataReceivedFromSoundcard_Client);

                        //Start recorder
                        if (_Recorder_Client.Start(_Config.SoundInputDeviceNameClient, _Config.SamplesPerSecondClient, _Config.BitsPerSampleClient, _Config.ChannelsClient, _SoundBufferCount, bufferSize))
                        {

                            //If jitterbuffer
                            if (UseJitterBufferClientRecording)
                            {
                                _JitterBufferClientRecording.Start();
                            }
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                ShowError(Clientnotification, ColorClientnotification, ex.Message);
            }
        }

        /// <summary>
        /// OnDataReceivedFromSoundcard_Client
        /// </summary>
        /// <param name="linearData"></param>
        private void OnDataReceivedFromSoundcard_Client(Byte[] data)
        {
            try
            {
                lock (this)
                {
                    if (IsClientConnected)
                    {
                        //Disassemble sound data into smaller parts
                        int bytesPerInterval = WinSound.Utils.GetBytesPerInterval((uint)_Config.SamplesPerSecondClient, _Config.BitsPerSampleClient, _Config.ChannelsClient);
                        int count = data.Length / bytesPerInterval;
                        int currentPos = 0;
                        for (int i = 0; i < count; i++)
                        {
                            //Convert section to RTP packet
                            Byte[] partBytes = new Byte[bytesPerInterval];
                            Array.Copy(data, currentPos, partBytes, 0, bytesPerInterval);
                            currentPos += bytesPerInterval;
                            WinSound.RTPPacket rtp = ToRTPPacket(partBytes, _Config.BitsPerSampleClient, _Config.ChannelsClient);

                            //If jitterbuffer
                            if (UseJitterBufferClientRecording)
                            {
                                //In Buffer legen
                                _JitterBufferClientRecording.AddData(rtp);
                            }
                            else
                            {
                                //Alles in RTP Packet umwandeln
                                Byte[] rtpBytes = ToRTPData(data, _Config.BitsPerSampleClient, _Config.ChannelsClient);
                                //AbSend
                                _Client.Send(_PrototolClient.ToBytes(rtpBytes));
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine(ex.Message);
            }
        }

        /// <summary>
        /// StopPlayingToSounddevice_Client
        /// </summary>
        private void StopPlayingToSounddevice_Client()
        {
            if (_PlayerClient != null)
            {
                _PlayerClient.Close();
                _PlayerClient = null;
            }

            //JitterBuffer break up
            if (_JitterBufferClientPlaying != null)
            {
                _JitterBufferClientPlaying.Stop();
            }
        }
        #endregion
        #endregion
        ////public override void Cleanup()
        ////{
        ////    // Clean up if needed

        ////    base.Cleanup();
        ////}
    }
}