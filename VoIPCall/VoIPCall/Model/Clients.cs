﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using System.Net.NetworkInformation;

namespace VoIPCall.Model
{
    public class Clients
    {
        public Clients(String server, int port)
        {
            //Set data
            this._Server = server;
            this._Port = port;

            //Attach events
            this.ExceptionAppeared += new DelegateException(this.OnExceptionAppeared);
            this.ClientConnected += new DelegateConnection(this.OnConnected);
            this.ClientDisconnected += new DelegateConnection(this.OnDisconnected);
        }

        //Attributes
        public TcpClient Client;
        NetworkStream _NetStream;
        byte[] _ByteBuffer;
        String _Server;
        int _Port;
        bool _AutoConnect = false;
        private System.Threading.Timer _TimerAutoConnect;
        private int _AutoConnectInterval = 10;

        /// <summary>
        /// ToString
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return String.Format("{0} {1}:{2}", this.GetType(), this._Server, this._Port);
        }

        /// <summary>
        /// Locker class for AutoConnect
        /// </summary>
        private class Locker_AutoConnectClass
        {
        }
        private Locker_AutoConnectClass Locker_AutoConnect = new Locker_AutoConnectClass();


        //Delegates or events
        public delegate void DelegateDataReceived(Clients client, Byte[] bytes);
        public delegate void DelegateDataSend(Clients client, Byte[] bytes);
        public delegate void DelegateDataReceivedComplete(Clients client, String message);
        public delegate void DelegateConnection(Clients client, string Info);
        public delegate void DelegateException(Clients client, Exception ex);
        public event DelegateDataReceived DataReceived;
        public event DelegateDataSend DataSend;
        public event DelegateConnection ClientConnected;
        public event DelegateConnection ClientDisconnected;
        public event DelegateException ExceptionAppeared;

        /// <summary>
        /// Initialize timer for AutoConnect
        /// </summary>
        private void InitTimerAutoConnect()
        {
            //Wenn AutoConnect
            if (_AutoConnect)
            {
                if (_TimerAutoConnect == null)
                {
                    if (_AutoConnectInterval > 0)
                    {
                        _TimerAutoConnect = new System.Threading.Timer(new System.Threading.TimerCallback(OnTimer_AutoConnect), null, _AutoConnectInterval * 1000, _AutoConnectInterval * 1000);
                    }
                }
            }
        }
        /// <summary>
        /// send data
        /// </summary>
        /// <param name="Data"></param>
        public void Send(Byte[] data)
        {
            try
            {
                // Send the coded string to the _server
                _NetStream.Write(data, 0, data.Length);

                //Send event
                if (this.DataSend != null)
                {
                    this.DataSend(this, data);
                }
            }
            catch (Exception ex)
            {
                //Exception Send event
                ExceptionAppeared(this, ex);
            }
        }
        /// <summary>
        /// Starts the reading
        /// </summary>
        /// <param name="Data"></param>
        private void StartReading()
        {
            try
            {
                _ByteBuffer = new byte[1024];
                _NetStream.BeginRead(_ByteBuffer, 0, _ByteBuffer.Length, new AsyncCallback(OnDataReceived), _NetStream);
            }
            catch (Exception ex)
            {
                //Exception Send event
                ExceptionAppeared(this, ex);
            }
        }
        /// <summary>
        /// Called when data was received
        /// </summary>
        /// <param name="result"></param>
        private void OnDataReceived(IAsyncResult ar)
        {
            try
            {
                //Determine network stream
                NetworkStream myNetworkStream = (NetworkStream)ar.AsyncState;

                //Check network stream
                if (myNetworkStream.CanRead)
                {
                    //Daten lesen
                    int numberOfBytesRead = myNetworkStream.EndRead(ar);

                    //If data exists
                    if (numberOfBytesRead > 0)
                    {
                        //Send event
                        if (this.DataReceived != null)
                        {
                            //Detect only read bytes
                            Byte[] data = new byte[numberOfBytesRead];
                            System.Array.Copy(_ByteBuffer, 0, data, 0, numberOfBytesRead);

                            //Send
                            this.DataReceived(this, data);
                        }
                    }
                    else
                    {
                        //Send event
                        if (this.ClientDisconnected != null)
                        {
                            this.ClientDisconnected(this, "FIN");
                        }

                        //If not AutoConnect
                        if (_AutoConnect == false)
                        {
                            this.disconnect_intern();
                        }
                        else
                        {
                            this.Disconnect_ButAutoConnect();
                        }

                        //Finished
                        return;
                    }

                    //New reading process
                    myNetworkStream.BeginRead(_ByteBuffer, 0, _ByteBuffer.Length, new AsyncCallback(OnDataReceived), myNetworkStream);
                }
            }
            catch (Exception ex)
            {
                //Exception Send event
                ExceptionAppeared(this, ex);
            }
        }
        /// <summary>
        /// Neu Verbinden
        /// </summary>
        public void ReConnect()
        {
            //End connection
            this.Disconnect();
            //Start new connection
            this.Connect();
        }
        /// <summary>
        /// building connections
        /// </summary>
        public void Connect()
        {
            try
            {
                //Possibly. Enable AutoConnect
                InitTimerAutoConnect();

                // Create new socket bound to the _Server and _Port
                Client = new TcpClient(this._Server, this._Port);
                _NetStream = Client.GetStream();

                //Beginning to read
                this.StartReading();

                //Send event
                ClientConnected(this, String.Format("server: {0} port: {1}", this._Server, this._Port));
            }
            catch (Exception ex)
            {
                //Hand off
                throw ex;
            }
        }
        /// <summary>
        /// Ping the server of the client
        /// </summary>
        public void Ping()
        {
            Ping ping = new Ping();
            PingReply reply = ping.Send(_Server);
            if (reply.Status != IPStatus.Success)
            {
                throw new Exception(String.Format("Server {0} antwortet nicht auf Ping ", _Server));
            }
        }
        /// <summary>
        /// Ping the server of the client.Specification of the maximum waiting time in milliseconds.
        /// </summary>
        /// <param name="waitTimeout"></param>
        public void Ping(Int32 waitTimeout)
        {
            Ping ping = new Ping();
            PingReply reply = ping.Send(_Server, waitTimeout);
            if (reply.Status != IPStatus.Success)
            {
                throw new Exception(String.Format("Server {0} antwortet nicht auf Ping ", _Server));
            }
        }
        /// <summary>
        /// End connection
        /// </summary>
        public void Disconnect()
        {
            //End connection
            disconnect_intern();

            //If not already finished
            if (_TimerAutoConnect != null)
            {
                //Do not reconnect
                _TimerAutoConnect.Dispose();
                _TimerAutoConnect = null;
            }

            //Send event
            if (this.ClientDisconnected != null)
            {
                this.ClientDisconnected(this, "Verbindung beendet");
            }
        }
        /// <summary>
        /// End connection, but retain AutoConnect
        /// </summary>
        private void Disconnect_ButAutoConnect()
        {
            //End connection
            disconnect_intern();
        }
        /// <summary>
        /// End connection (intern)
        /// </summary>
        private void disconnect_intern()
        {
            if (Client != null)
            {
                Client.Close();
            }
            if (_NetStream != null)
            {
                _NetStream.Close();
            }
        }
        /// <summary>
        /// Timer that controls automatic connection
        /// </summary>
        /// <param name="ob"></param>
        private void OnTimer_AutoConnect(Object ob)
        {
            try
            {
                lock (Locker_AutoConnect)
                {
                    //If desired
                    if (_AutoConnect)
                    {
                        //If not connected
                        if (Client == null || Client.Connected == false)
                        {
                            //Create new socket bound to the _Server and _Port
                            Client = new TcpClient(this._Server, this._Port);
                            _NetStream = Client.GetStream();

                            //Beginning to read
                            this.StartReading();

                            //Send event
                            ClientConnected(this, String.Format("server: {0} port: {1}", this._Server, this._Port));
                        }
                    }
                    else
                    {
                        if (_TimerAutoConnect != null)
                        {
                            //End timer
                            _TimerAutoConnect.Dispose();
                            _TimerAutoConnect = null;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                //Exception Send event
                ExceptionAppeared(this, ex);
            }
        }
        /// <summary>
        /// If an exception happens
        /// </summary>
        /// <param name="ex"></param>
        private void OnExceptionAppeared(Clients client, Exception ex)
        {

        }
        /// <summary>
        /// When a client has joined
        /// </summary>
        /// <param name="client"></param>
        private void OnConnected(Clients client, string info)
        {

        }
        /// <summary>
        /// When a client has disconnected
        /// </summary>
        /// <param name="client"></param>
        private void OnDisconnected(Clients client, string info)
        {

        }
        /// <summary>
        /// Interval for AutoConnect in seconds
        /// </summary>
        public Int32 AutoConnectInterval
        {
            get
            {
                return _AutoConnectInterval;
            }
            set
            {
                _AutoConnectInterval = value;

                //Check
                if (value > 0)
                {
                    try
                    {
                        //When schnon active
                        if (_TimerAutoConnect != null)
                        {
                            //To change
                            _TimerAutoConnect.Change(value * 1000, value * 1000);
                        }
                    }
                    catch (Exception ex)
                    {
                        ExceptionAppeared(this, ex);
                    }
                }
            }
        }
        /// <summary>
        /// Controls automatic reconnection
        /// </summary>
        /// <returns></returns>
        public bool AutoConnect
        {
            get
            {
                return _AutoConnect;
            }
            set
            {
                _AutoConnect = value;

                if (value == true)
                {
                    InitTimerAutoConnect();
                }

            }
        }
        /// <summary>
        /// Indicates if the client is trying to connect via AutoConnect
        /// </summary>
        public bool IsRunning
        {
            get
            {
                return _TimerAutoConnect != null;
            }
        }
        /// <summary>
        /// Indicates if the client is connected. Read Only
        /// </summary>
        /// <returns></returns>
        public bool Connected
        {
            get
            {
                if (this.Client != null)
                {
                    return this.Client.Connected;
                }
                else
                {
                    return false;
                }
            }
        }

        public Byte[] recevie()
        {
            Byte[] data = new Byte[1024 * 5000];
            _NetStream.Read(data, 0, data.Length);
            return data;
        }
    }
}
