﻿using GalaSoft.MvvmLight;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VoIPCall.Model
{
    public class ClientConnected : ViewModelBase
    {
        /// <summary>
        /// The <see cref="NameIPConnected" /> property's name.
        /// </summary>
        public const string NameIPConnectedPropertyName = "NameIPConnected";

        private string _NameIPConnected = string.Empty;

        /// <summary>
        /// Sets and gets the NameIPConnected property.
        /// Changes to that property's value raise the PropertyChanged event. 
        /// </summary>
        public string NameIPConnected
        {
            get
            {
                return _NameIPConnected;
            }

            set
            {
                if (_NameIPConnected == value)
                {
                    return;
                }

                _NameIPConnected = value;
                RaisePropertyChanged(NameIPConnectedPropertyName);
            }
        }

        /// <summary>
        /// The <see cref="IDAddress" /> property's name.
        /// </summary>
        public const string IDAddressPropertyName = "IDAddress";

        private string _IDAddress = string.Empty;

        /// <summary>
        /// Sets and gets the IDAddress property.
        /// Changes to that property's value raise the PropertyChanged event. 
        /// </summary>
        public string IDAddress
        {
            get
            {
                return _IDAddress;
            }

            set
            {
                if (_IDAddress == value)
                {
                    return;
                }

                _IDAddress = value;
                RaisePropertyChanged(IDAddressPropertyName);
            }

        }

        /// <summary>
        /// The <see cref="StatusClient" /> property's name.
        /// </summary>
        public const string StatusClientPropertyName = "StatusClient";

        private string _StatusClient = string.Empty;

        /// <summary>
        /// Sets and gets the StatusClient property.
        /// Changes to that property's value raise the PropertyChanged event. 
        /// </summary>
        public string StatusClient
        {
            get
            {
                return _StatusClient;
            }

            set
            {
                if (_StatusClient == value)
                {
                    return;
                }

                _StatusClient = value;
                RaisePropertyChanged(StatusClientPropertyName);
            }
        }
    }
}
