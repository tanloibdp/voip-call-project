﻿using System;
using System.Net;
using System.Net.Sockets;

namespace VoIPCall.Model
{
    public class DataService : IDataService
    {
        public void GetServer(Action<DataIP_PORT, Exception> callback)
        {
            string IP = string.Empty;
            string Port = "0";
            // Use this to create design time data
            IPAddress[] localIP = Dns.GetHostAddresses(Dns.GetHostName());
            foreach (IPAddress address in localIP)
            {
                if (address.AddressFamily == AddressFamily.InterNetwork)
                {
                    IP = address.ToString();
                }
            }

            var item = new DataIP_PORT(IP, Port);
            callback(item, null);
        }
        public void GetButtonServer(Action<DataItem, Exception> callback)
        {
            string Text = "_START";
            string Color = "#FF2196F3";

            var item = new DataItem(Text, Color);
            callback(item, null);
        }
        public void GetButtonClient(Action<DataItem, Exception> callback)
        {
            string Text = "_CONNECT";
            string Color = "#FF2196F3";

            var item = new DataItem(Text, Color);
            callback(item, null);
        }
        public void GetNotification(Action<DataItem, Exception> callback)
        {
            string Text = "";
            string Color = "#000000";

            var item = new DataItem(Text, Color);
            callback(item, null);
        }
    }
}