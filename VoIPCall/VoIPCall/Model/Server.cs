﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Xml.Serialization;
using System.Threading;
using System.Collections;

namespace VoIPCall.Model
{
    public class Server
    {
        /// <summary>
        /// Constructor
        /// </summary>
        public Server()
        {

        }

        //Attributes
        private IPEndPoint _endpoint;
        private TcpListener _tcpip;
        private Thread _ThreadMainServer;
        private ListenerState _State;


        //The list of running TCPServer threads
        private List<ServerThread> _threads = new List<ServerThread>();

        //Delegates
        public delegate void DelegateClientConnected(ServerThread st);
        public delegate void DelegateClientDisconnected(ServerThread st, string info);
        public delegate void DelegateDataReceived(ServerThread st, Byte[] data);

        //Events
        public event DelegateClientConnected ClientConnected;
        public event DelegateClientDisconnected ClientDisconnected;
        public event DelegateDataReceived DataReceived;

        /// <summary>
        /// TCPServer Stati
        /// </summary>
        public enum ListenerState
        {
            None,
            Started,
            Stopped,
            Error
        };
        /// <summary>
        /// All current clients of the server
        /// </summary>
        public List<ServerThread> Clients
        {
            get
            {
                return _threads;
            }
        }
        /// <summary>
        /// Connected
        /// </summary>
        public ListenerState State
        {
            get
            {
                return _State;
            }
        }
        /// <summary>
        /// Returns the inner TcpListener of the server
        /// </summary>
        public TcpListener Listener
        {
            get
            {
                return this._tcpip;
            }
        }
        /// <summary>
        /// Start the server
        /// </summary>
        public void Start(string strIPAdress, int Port)
        {
            //Determine endpoint and listener
            _endpoint = new IPEndPoint(IPAddress.Parse(strIPAdress), Port);
            _tcpip = new TcpListener(_endpoint);

            if (_tcpip == null) return;

            try
            {
                _tcpip.Start();

                // Initialize and start the main TCPServer thread
                _ThreadMainServer = new Thread(new ThreadStart(Run));
                _ThreadMainServer.IsBackground = true;
                _ThreadMainServer.Start();

                //State
                this._State = ListenerState.Started;
            }
            catch (Exception ex)
            {
                //break up
                _tcpip.Stop();
                this._State = ListenerState.Error;

                //Throw an exception
                throw ex;
            }
        }
        /// <summary>
        /// Run
        /// </summary>
        private void Run()
        {
            while (true)
            {
                try
                {
                    //Waiting for incoming connection request
                    TcpClient client = _tcpip.AcceptTcpClient();
                    //Initializes and starts a TCPServer thread
                    //and add it to the list of TCPServer threads
                    ServerThread st = new ServerThread(client);

                    //Add events
                    st.DataReceived += new ServerThread.DelegateDataReceived(OnDataReceived);
                    st.ClientDisconnected += new ServerThread.DelegateClientDisconnected(OnClientDisconnected);

                    //Further work
                    OnClientConnected(st);

                    try
                    {
                        //Starting to read
                        client.Client.BeginReceive(st.ReadBuffer, 0, st.ReadBuffer.Length, SocketFlags.None, st.Receive, client.Client);
                    }
                    catch (Exception ex)
                    {
                        //Connection faulty
                        Console.WriteLine(ex.Message);
                    }
                }
                catch (Exception)
                {
                    break;
                }
            }
        }

        /// <summary>
        /// Send message to all connected clients. Returns the number of existing clients
        /// </summary>
        /// <param name="Message"></param>
        public int Send(Byte[] data)
        {
            //For every connection
            List<ServerThread> list = new List<ServerThread>(_threads);
            foreach (ServerThread sv in list)
            {
                try
                {
                    //Send
                    if (data.Length > 0)
                    {
                        sv.Send(data);
                    }
                }
                catch (Exception)
                {

                }
            }
            //Return number
            return _threads.Count;
        }
        /// <summary>
        /// Is executed when data has arrived
        /// </summary>
        /// <param name="Data"></param>
        private void OnDataReceived(ServerThread st, Byte[] data)
        {
            //Send event or hand off
            if (DataReceived != null)
            {
                DataReceived(st, data);
            }
        }
        /// <summary>
        /// Called when a client exits
        /// </summary>
        /// <param name="st"></param>
        private void OnClientDisconnected(ServerThread st, string info)
        {
            //Remove from list
            _threads.Remove(st);

            //Send event or hand off
            if (ClientDisconnected != null)
            {
                ClientDisconnected(st, info);
            }
        }
        /// <summary>
        /// Called when a client connects
        /// </summary>
        /// <param name="st"></param>
        private void OnClientConnected(ServerThread st)
        {
            //If not available
            if (!_threads.Contains(st))
            {
                //Add to the list of clients
                _threads.Add(st);
            }

            //Send event or hand off
            if (ClientConnected != null)
            {
                ClientConnected(st);
            }
        }
        /// <summary>
        /// break up the server
        /// </summary>
        public void Stop()
        {
            try
            {
                // Stop all TCPServer threads
                for (IEnumerator en = _threads.GetEnumerator(); en.MoveNext();)
                {
                    //Get next TCPServer thread
                    ServerThread st = (ServerThread)en.Current;
                    //and stop
                    st.Stop();

                    //Send event
                    if (ClientDisconnected != null)
                    {
                        ClientDisconnected(st, "Connection has ended");
                    }
                }

                if (_tcpip != null)
                {
                    //Stop Listener
                    _tcpip.Stop();
                    _tcpip.Server.Close();
                }

                //if (_ThreadMainServer != null)
                //{
                //    // Stop main TCPServer thread
                //    _ThreadMainServer.Abort();
                //    System.Threading.Thread.Sleep(100);
                //}

                //Note status
                this._State = ListenerState.Stopped;

            }
            catch (Exception)
            {
                this._State = ListenerState.Error;
            }
        }

        public Byte[] recevie(TcpClient client)
        {
            Byte[] data = new Byte[1024 * 5000];
            NetworkStream network = new NetworkStream(client.Client);
            int rec = network.Read(data, 0, data.Length);
            return data;
        }
    }

    /// <summary>
    /// Server thread of a server
    /// </summary>
    public class ServerThread
    {
        // Stop flag
        private bool _IsStopped = false;
        // The connection to the client
        private TcpClient _Connection = null;
        //read buffer
        public byte[] ReadBuffer = new byte[1024];
        //Mute
        public bool IsMute = false;
        //Name
        public String Name = "";

        public delegate void DelegateDataReceived(ServerThread st, Byte[] data);
        public event DelegateDataReceived DataReceived;
        public delegate void DelegateClientDisconnected(ServerThread sv, string info);
        public event DelegateClientDisconnected ClientDisconnected;

        /// <summary>
        /// Inside client
        /// </summary>
        public TcpClient Client
        {
            get
            {
                return _Connection;
            }
        }
        /// <summary>
        /// Connection is completed
        /// </summary>
        public bool IsStopped
        {
            get
            {
                return _IsStopped;
            }
        }
        // Save the connection to the client and start the thread
        public ServerThread(TcpClient connection)
        {
            // saves the connection to client,
            // to be able to close them later
            this._Connection = connection;
        }
        /// <summary>
        /// Read news
        /// </summary>
        /// <param name="ar"></param>
        public void Receive(IAsyncResult ar)
        {
            try
            {
                //If not connected anymore
                if (this._Connection.Client.Connected == false)
                {
                    return;
                }

                if (ar.IsCompleted)
                {
                    //Read
                    int bytesRead = _Connection.Client.EndReceive(ar);

                    //If data exists
                    if (bytesRead > 0)
                    {
                        //Detect only read bytes
                        Byte[] data = new byte[bytesRead];
                        System.Array.Copy(ReadBuffer, 0, data, 0, bytesRead);

                        //Send event
                        DataReceived(this, data);
                        //Continue reading
                        _Connection.Client.BeginReceive(ReadBuffer, 0, ReadBuffer.Length, SocketFlags.None, Receive, _Connection.Client);
                    }
                    else
                    {
                        //connection lost
                        HandleDisconnection("Connection has ended");
                    }
                }
            }
            catch (Exception ex)
            {
                //connection lost
                HandleDisconnection(ex.Message);
            }
        }
        /// <summary>
        /// Do everything necessary to terminate a connection
        /// </summary>
        public void HandleDisconnection(string reason)
        {
            //Client Connection is completed
            _IsStopped = true;

            //Send event
            if (ClientDisconnected != null)
            {
                ClientDisconnected(this, reason);
            }
        }
        /// <summary>
        /// Send messages
        /// </summary>
        /// <param name="strMessage"></param>
        public void Send(Byte[] data)
        {
            try
            {
                //If the connection still exists
                if (this._IsStopped == false)
                {
                    //Get the stream for writing
                    NetworkStream ns = this._Connection.GetStream();

                    lock (ns)
                    {
                        // Send the coded string to the TCPServer
                        ns.Write(data, 0, data.Length);
                    }
                }
            }
            catch (Exception ex)
            {
                //Close connection
                this._Connection.Close();
                //End connection
                this._IsStopped = true;

                //Send event
                if (ClientDisconnected != null)
                {
                    ClientDisconnected(this, ex.Message);
                }

                //Forward Exception
                throw ex;
            }
        }
        /// <summary>
        /// Stop the thread
        /// </summary>
        public void Stop()
        {
            //If a client is still connected
            if (_Connection.Client.Connected == true)
            {
                //End connection
                _Connection.Client.Disconnect(false);
            }

            this._IsStopped = true;
        }
    }
}
