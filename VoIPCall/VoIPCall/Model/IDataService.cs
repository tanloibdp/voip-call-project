﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace VoIPCall.Model
{
    public interface IDataService
    {
        void GetServer(Action<DataIP_PORT, Exception> callback);
        void GetNotification(Action<DataItem, Exception> callback);
        void GetButtonServer(Action<DataItem, Exception> callback);
        void GetButtonClient(Action<DataItem, Exception> callback);
    }
}
