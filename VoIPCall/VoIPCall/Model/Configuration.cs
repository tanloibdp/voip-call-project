﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConvertTCPStream.Model
{
    public class Configuration
    {
        /// <summary>
		/// Config
		/// </summary>
		public Configuration()
        {

        }

        //Attributes
        public String IpAddressClient = "";
        public String IPAddressServer = "";
        public int PortClient = 0;
        public int PortServer = 0;
        public String SoundInputDeviceNameClient = "";
        public String SoundOutputDeviceNameClient = "";
        public String SoundInputDeviceNameServer = "";
        public String SoundOutputDeviceNameServer = "";
        public int SamplesPerSecondClient = 8000;
        public int BitsPerSampleClient = 16;
        public int ChannelsClient = 1;
        public int SamplesPerSecondServer = 8000;
        public int BitsPerSampleServer = 16;
        public int ChannelsServer = 1;
        public bool UseJitterBufferClientRecording = true;
        public bool UseJitterBufferServerRecording = true;
        public uint JitterBufferCountServer = 20;
        public uint JitterBufferCountClient = 20;
    }
}
