﻿namespace VoIPCall.Model
{
    public class DataItem
    {
        public string Color
        {
            get;
            private set;
        }

        public string Text
        {
            get;
            private set;
        }

        public DataItem(string Text, string Color)
        {
            this.Text = Text;
            this.Color = Color;
        }
    }

    public class DataIP_PORT
    {
        public string IP
        {
            get;
            private set;
        }

        public string PORT
        {
            get;
            private set;
        }

        public DataIP_PORT(string IP, string PORT)
        {
            this.IP = IP;
            this.PORT = PORT;
        }
    }
}